SO_SOURCE=5r-native-plugins
SO_TARGET=5r-worker/src/main/resources/linux-x86-64
JNI_TARGET=5r-worker/src/main/java/org/fivr/worker/plugin/cxx

HOST ?= 5r-master
DEPLOY_USER ?= user

all: 5r-worker 5r-web dist

.PHONY: 5r-worker webstart dist deploy 5r-web

# Setup all required dependencies on developer's machine
installDependencies:
	sudo apt-get install sudo cmake g++ libopencv-dev libjsoncpp-dev maven python3 redis-server redis-tools default-jdk php python3 python3-setuptools rsync zip unzip php-zip

# Creates a new plug-in
newplugin:
	cd $(SO_SOURCE); ../5r-plugin-wizard/5r-plugin-wizard.py $(PLUGIN)

# Pseudo target responsible for building all native modules
native-plugins:
	cd $(SO_SOURCE); ./build-all.sh

# Only builds the worker compnent
5r-worker: native-plugins
	cd 5r-worker; mvn compile assembly:single

# Only builds the web component
5r-web:
	find . -name "*.php" -print0 | xargs -0 -n1 -P8 php -l
	cd 5r-web; composer install

# Creates a ready to be deployed package
dist:
	mkdir -p target/5r-web
	rsync -av --exclude 'data/*' --exclude 'cache/*' --exclude '**/.git/*' 5r-web/ target/5r-web
	cp 5r-worker/target/*.jar target/5r-web/assets/5r-worker.jar
	tar -cvaf target/5r-web.tar.gz -C target/ 5r-web

# Deploy the currently build package to the master node
deployMaster:
	scp target/5r-web.tar.gz $(DEPLOY_USER)@$(HOST):/var/www/html/5r-web.tar.gz
	ssh $(DEPLOY_USER)@$(HOST) tar --strip-components 1 -xavf /var/www/html/5r-web.tar.gz -C /var/www/html
	ssh $(DEPLOY_USER)@$(HOST) sudo /var/www/html/assets/5r-master.sh

# Setup a fresh worker by installing a systemd service file
deployWorker:
	scp 5r-worker/5r-worker 5r-worker/5r-worker.service $(DEPLOY_USER)@$(HOST):/tmp
	ssh $(DEPLOY_USER)@$(HOST) sudo mkdir -p /usr/local/bin /etc/systemd/user
	ssh $(DEPLOY_USER)@$(HOST) sudo cp /tmp/5r-worker /usr/local/bin/5r-worker && sudo cp /tmp/5r-worker.service /etc/systemd/user
	ssh $(DEPLOY_USER)@$(HOST) sudo systemctl enable 5r-worker.service && sudo systemctl start 5r-worker.service

# Can be used quickly fire up a local web-server and test modifications
webstart:
	cd 5r-web; php -S 0.0.0.0:8080

# Can be used to quickly starte the recently build worker component.
workerstart:
	java -jar 5r-worker/target/*.jar

# Cleans up all generated build files
clean:
	rm -rf $(SO_TARGET)
	rm -rf $(JNI_TARGET)
	rm -f 5r-web/assets/5r-worker.jar
	rm -rf 5r-web/vendor
	rm -rf $(SO_SOURCE)/*-build
	cd 5r-worker; mvn clean
	rm -rf target
