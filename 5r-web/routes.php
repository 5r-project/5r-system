<?php

$app->get('/', function() use ($app){
    $app->redirect('dashboard');
});

//Main GET routes
$app->get('/dashboard', '\Fivr\Controller\Dashboard:index')->name('dashboard');
$app->get('/jobs', '\Fivr\Controller\Job:index')->name('jobs');
$app->get('/datasets', '\Fivr\Controller\Dataset:index')->name('datasets');

//plugins
$app->get('/plugins', '\Fivr\Controller\Plugins:index')->name('plugins');
$app->get('/plugins/clear', '\Fivr\Controller\Plugins:clear')->name('clearPlugins');

//worker
$app->get('/worker', '\Fivr\Controller\Worker:index')->name('worker');
$app->get('/worker/clearWorker', '\Fivr\Controller\Worker:clearWorker')->name('clearWorker');
$app->get('/worker/restart/:id', '\Fivr\Controller\Worker:restartWorker')->name('restartWorker');
$app->get('/worker/restartAll', '\Fivr\Controller\Worker:restartAllWorker')->name('restartAllWorker');


//dataset
$app->get('/datasets/import','\Fivr\Controller\Dataset:import')->name('importDataset');
$app->post('/datasets/import', '\Fivr\Controller\Dataset:processImport');
$app->get('/datasets/view/:id','\Fivr\Controller\Dataset:view');
$app->get('/datasets/delete/:id','\Fivr\Controller\Dataset:delete');

//analysis
$app->get('/analysis', '\Fivr\Controller\Analysis:index');
$app->post('/analysis/dataset', '\Fivr\Controller\Analysis:analyzeDataset');

//job
$app->get('/jobs/create', '\Fivr\Controller\Job:create')->name('createJob');
$app->get('/jobs/clearDone', '\Fivr\Controller\Job:clearDone')->name('clearDone');
$app->get('/jobs/clearEnqueued', '\Fivr\Controller\Job:clearEnqueued')->name('clearEnqueued');
$app->get('/jobs/abortJob/:id', '\Fivr\Controller\Job:abortJob')->name('abortJob');
$app->get('/jobs/abortAll', '\Fivr\Controller\Job:abortAll')->name('abortAll');
$app->get('/jobs/stats', '\Fivr\Controller\Job:stats')->name('stats');
$app->get('/jobs/stats/csv', '\Fivr\Controller\Job:statsCSV')->name('statsCSV');

//player
$app->get('/player', '\Fivr\Controller\Player:index')->name('player');
$app->get('/player/:dataset/:video', '\Fivr\Controller\Player:show');
