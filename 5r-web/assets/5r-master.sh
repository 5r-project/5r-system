#!/bin/bash
apt-get update -qq
apt-get install -qq php apache2 redis-server redis-tools nfs-kernel-server

cat /etc/exports | grep "FIVR-export"

if [ $? -eq 0 ]
then
	echo "Export already installed"
else
	echo "#FIVR-export" >> /etc/exports
	echo "/var/www/html/data 0.0.0.0/0(rw,async,no_subtree_check)" >> /etc/exports
	exportfs -a
fi

chmod -R a+rwx /var/www/html
rm -f /var/www/html/cache/*
