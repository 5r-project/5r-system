/**
 * Created by cinemast on 12/18/15.
 */
$(document).ready(function() {

    function jq( myid ) {
        return "#" + myid.replace( /(:|\.|\[|\]|,)/g, "\\$1" );
    }

    $(".paramGroup").hide();

    $(".pluginCheckbox").change(function() {
        //alert("something changed: " + $(this).attr('id'));
        var id = jq($(this).attr('id')+".params");
      //  alert(id);
        var obj = $(id);
        //alert(obj.attr('class'));
        $(id).fadeToggle('fast');

    });

    $('select').select2();
});