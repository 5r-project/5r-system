/**
 * Created by cinemast on 12/18/15.
 */

function setPlayerTime(time) {
    player.currentTime = time;
}

$(document).ready(function() {

    function jq( myid ) {
        return "#" + myid.replace( /(:|\.|\[|\]|,)/g, "\\$1" );
    }

    videoName = $("input#videoName").val();
    enableSelectedVisualisation();

    $('select').select2();

    $('select').change(function() {
        window.location.href = '/player/' + $(this).val();
    }) ;

    player = $('#videoPlayer')[0];

    $(".featureSelector").change(function() {
        enableSelectedVisualisation();
    });

    player.ontimeupdate = function () {
        $("#timeSpan").text(player.currentTime);
    };

    function enableSelectedVisualisation() {
        $(".featureVisualisation").hide('fast');
        var selectedVal = "";
        var selected = $("input[type='radio']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val();
        }
        $(jq(selectedVal)).show('fast');
    }


    //Custom feature implementations
    $("#nordlicht").click(function(e) {
        var offset = $(this).offset();
        var xOffset = e.pageX - offset.left;
        var progress = xOffset / $(this).width();
        console.log(progress);
        player.currentTime = player.duration * progress;
    });

    //Shotdetection
    $.getJSON("/data/"+videoName+"/cxx.ShotDetection/index.json").done(function(data) {
        $.each(data.shots, function(index, value) {
            $("#shotList").append('<li><a href="javascript:setPlayerTime('+value.time+');"><img width=100px src="/data/'+videoName + '/cxx.ShotDetection/'+value.image+'"/></a></li>');
        });


    });


});

