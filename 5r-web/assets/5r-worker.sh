#!/bin/bash
# Configuration variables
REDIS_HOST=127.0.0.1
DATA_HOST=127.0.0.1
DATA_PATH=/media/fivr

#Mount the data directory
sudo mkdir -p $DATA_PATH
sudo mount -t cifs -o username=guest,password= //$DATA_HOST/fivr $DATA_PATH

sudo apt-get install openjdk-7-jre libopencv-dev libjsoncpp0

#Download the latest release
wget http://REDIS_HOST/assets/5r-worker.jar

while(true)
do
	java -jar 5r-worker.jar $REDIS_HOST $DATA_PATH
done

sudo umount $DATA_PATH

