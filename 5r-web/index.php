<?php
require 'vendor/autoload.php';

$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Blade(),
    'templates.path' => './src/View',
    'mode' => 'production'
));

$view = $app->view()->parserOptions = array(
    'debug' => false,
    'cache' => 'cache'
);

require 'routes.php';


$app->run();

