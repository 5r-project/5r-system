<?php

namespace Fivr\Model
{
    class DatasetModel extends RedisModel
    {
        const BASEDIR = 'data';
        const KEY_ID = '5r:id:dataset';
        const KEY_DATASETS = '5r:datasets';
        const KEY_PREFIX = '5r:dataset:';


        public function getAllDatasets()
        {
            $result = array();
            $keys = $this->redis->zrange(self::KEY_DATASETS, 0, -1);
            foreach($keys as $key)
            {
                $result[] = $this->getDataset($key);
            }
            return $result;
        }

        public function getDataset($id) {
            $result = $this->redis->hgetall(self::KEY_PREFIX . $id);
            $result['id'] = $id;
            $result['videos'] = explode(";", substr($result['videos'], 0, strlen($result['videos'])-1));
            $result['encodings'] = explode(";", substr($result['encodings'], 0, strlen($result['encodings'])-1));
            return $result;
        }

        public function getVideos($datasetId)
        {
            $dataset = $this->getDataset($datasetId);

            $videos = [];

            foreach($dataset['videos'] as $video)
            {
                $videos[] = $this->getVideo($dataset['directory'] . "/" . $video);
            }
            return $videos;
        }

        public function getDirectoryFiles($directory)
        {
            $files = [];

            foreach(scandir(self::BASEDIR. '/'. $directory) as $entry)
            {
                $path = self::BASEDIR . '/' . $directory . '/' . $entry;
                if (is_dir($path) and $entry != '.' and $entry != '..')
                {
                    foreach(scandir($path) as $file)
                    {
                        if (is_file($path . '/' . $file) and $file != '.' and $file != '..')
                        {
                            $files[$path."/".$file] = $entry ."/" .$file;
                        }

                    }
                }

            }
            return $files;
        }

        public function getAllVideosAndDatasets()
        {
            $datasets = $this->getAllDatasets();
            $options = [];
            if (count($datasets) > 0) {
                foreach ($datasets as $dataset) {
                    foreach ($dataset['encodings'] as $encoding) {
                        $options[$dataset['directory'] . '/' . $encoding] = $dataset['directory'] . ' @ ' . $encoding;

                        foreach ($dataset['videos'] as $video) {
                            $options[$dataset['directory'] . '/' . $video . '/encodings/' . $encoding] = $dataset['directory'] . '/' . $video . '/' . $encoding;
                        }
                    }
                }
            }
            return $options;
        }

        public function getAllVideos()
        {
            $datasets = $this->getAllDatasets();
            $options = [];
            if (count($datasets) > 0) {
                foreach ($datasets as $dataset) {
                    foreach ($dataset['videos'] as $video) {
                        $options[$dataset['directory'] . '/' . $video] = $dataset['directory'] . '/' . $video;
                    }
                }
            }
            return $options;
        }

        public function getImportCandidates()
        {
            $result = array();
            $dirEntries = scandir(self::BASEDIR);
            foreach($dirEntries as $entry)
            {
                if ($entry != '.' and $entry != '..' and is_dir(self::BASEDIR.'/'.$entry))
                {
                    if ($this->redis->zscore(self::KEY_DATASETS, $entry) == NULL)
                        $result[] = $entry;
                }
            }
            return $result;
        }

        public function deleteDataset($id)
        {
            $ds = $this->getDataset($id);
            $this->redis->del(self::KEY_PREFIX. $id);
            $this->redis->zrem(self::KEY_DATASETS, $id);
            delete_directory(self::BASEDIR . '/' . $ds['directory']);
        }

        public function getVideo($path)
        {

            $videoPath = self::BASEDIR . '/' . $path;
            $video = [];
            if (is_dir($videoPath))
            {
                $features = [];
                $dirHandle = opendir($videoPath);
                while($file = readdir($dirHandle))
                {
                    if ($file != 'encodings' && $file != "." && $file != "..")
                    {
                        $features[] = $file;
                    }
                }
                closedir($dirHandle);
                $encodings = [];
                $dirHandle = opendir($videoPath . '/encodings');
                while ($file = readdir($dirHandle))
                {
                    if ($file != "." && $file != "..") {
                        $encodings[] = $file;
                    }
                }
                closedir($dirHandle);

                $video = [
                    'name' => $path,
                    'encodings' => $encodings,
                    'features' => $features,
                    'files' => $this->getDirectoryFiles($path)
                ];
            }
            return $video;
        }

    }



    function delete_directory($dirname) {
        if (is_dir($dirname))
        {
            $dir_handle = opendir($dirname);

            while($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (!is_dir($dirname."/".$file))
                        unlink($dirname."/".$file);
                    else
                        delete_directory($dirname.'/'.$file);
                }
            }
            closedir($dir_handle);
            rmdir($dirname);
            return true;
        }
        return false;
    }
}

