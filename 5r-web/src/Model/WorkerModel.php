<?php
namespace Fivr\Model;

class WorkerModel extends RedisModel
{
    public function getAllWorker()
    {
        $keys = $this->redis->keys("5r:worker:*");
        $result = array();
        foreach($keys as $key) {
            $result[] = $this->redis->hgetall($key);
        }
        return $result;
    }

    public function clearWorker()
    {
        $this->redis->set('5r:id:worker', 0);
        $keys = $this->redis->keys("5r:worker:*");
        foreach($keys as $key) {
            $this->redis->del($key);
        }
        
        $keys = $this->redis->keys("5r:control:*");
        foreach($keys as $key) {
            $this->redis->del($key);
        }

    }
    
    public function restartWorker($id) {
    	$this->redis->rpush("5r:control:".$id, "restart");
    }
}
