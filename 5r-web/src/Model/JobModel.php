<?php
namespace Fivr\Model;

class JobModel extends RedisModel
{
    public function addJob($plugin, array $params, $outputDirectory, $overallJob)
    {
        $id = $this->redis->incr('5r:id:job');

        $paramString = "";
        foreach($params as $key => $value)
        {
            $paramString .= $key . ':' . $value . ';';
        }

        $job = [
            'id' => $id,
            'plugin' => $plugin,
            'params' => $paramString,
            'directory' => $outputDirectory,
            'state' => "WAITING",
            'timeStart' => "",
            'timeStop' => "",
            'progress' => 0,
            'files' => '',
            'info' => '',
			'overallJob' => $overallJob
        ];

        $this->redis->hmset('5r:job:'.$id, $job);
        $this->redis->rpush('5r:queue:todo', $id);
        return $id;
    }

    public function getQueueJobs($queue = 'processing')
    {
        $keys = $this->redis->lrange('5r:queue:' . $queue, 0, -1);
        $result = array();
        foreach($keys as $key)
        {
            $result[] = $this->redis->hgetall('5r:job:'.$key);
        }

        return $result;
    }

    public function getAllJobs()
    {
        $processing = $this->getQueueJobs('processing');
        $todo = $this->getQueueJobs('todo');
        $done = $this->getQueueJobs('done');
        $result = array_merge($processing, $todo, $done);
        return $result;
    }

    public function abortJob($jobId)
    {
        $this->redis->sadd('5r:queue:abort', $jobId);
    }

    public function abortAllJobs()
    {
        $todoKeys = $this->redis->lrange('5r:queue:todo', 0, -1);
        $processingKeys = $this->redis->lrange('5r:queue:todo', 0, -1);
        $keys = array_merge($todoKeys, $processingKeys);

        foreach($keys as $key) {
            $this->abortJob($key);
        }
    }

    public function clearJobs()
    {
        $this->clearQueue('todo');
        $this->clearQueue('processing');
        $this->clearQueue('done');
        $this->redis->set('5r:id:job', 0);
    }

    public function clearQueue($queue)
    {
        $jobs = $this->redis->lrange('5r:queue:'.$queue, 0, -1);
        foreach($jobs as $key)
        {
            $this->redis->del('5r:job:'.$key);
        }
        $this->redis->del('5r:queue:'.$queue);
    }


}
