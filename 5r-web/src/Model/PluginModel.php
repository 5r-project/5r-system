<?php

namespace Fivr\Model
{
    class PluginModel extends RedisModel
    {
        const PLUGIN_PREFIX = "5r:plugin:*";

        public function getAllPlugins()
        {
            $keys = $this->redis->smembers('5r:plugins');
            $result = array();
            foreach($keys as $key) {
                $result[] = $this->redis->hgetall('5r:plugin:'.$key);
            }

            foreach ($result as &$item) {
                $item['params'] = $this->convertParams($item['params']);
            }
            return $result;
        }

        public function clearPlugins()
        {
            $keys = $this->redis->keys('5r:plugin:*');
            foreach($keys as $key)
            {
                $this->redis->del($key);
            }
            $this->redis->del('5r:plugins');
        }

        public function getPreprocessingPlugins()
        {
            $plugins = $this->getAllPlugins();

            foreach($plugins as $key => $value)
            {
                if ($value['type'] != 'preprocessing')
                    unset($plugins[$key]);
            }
            return $plugins;
        }

        public function getAnalysisPlugins()
        {
            $plugins = $this->getAllPlugins();

            foreach($plugins as $key => $value)
            {
                if ($value['type'] != 'analysis')
                    unset($plugins[$key]);
            }
            return $plugins;
        }

        private function convertTypeName($type)
        {
            switch($type) {
                case 1: return "String";
                case 2: return "Integer";
                case 3: return "Long";
                case 4: return "Double";
                case 5: return "Bool";
                case 6: return "File/Path";
                case 7: return "Enum";
                default: return "invalid";
            }
        }

        private function convertParams($param)
        {
            $result = array();
            $keys = explode(";", $param);
            foreach($keys as $key)
            {
                $parts = explode(":",$key);
                if (count($parts) == 3) {
                    $typeName = $this->convertTypeName($parts[1]);
                    $result[] = array(
                        'name' => $parts[0],
                        'type' => $parts[1],
                        'value' => explode(",",$parts[2]),
                        'typeName' => $typeName
                    );
                } else if(count($parts) == 2) {
					$typeName = $this->convertTypeName($parts[1]);
                    $result[] = array(
                        'name' => $parts[0],
                        'type' => $parts[1],
                        'value' => array(),
                        'typeName' => $typeName
                    );
				}
            }
            return $result;
        }

        public function getPlugin($pluginId)
        {
            $plugin = $this->redis->hgetall('5r:plugin:'.$pluginId);
            $plugin['params'] = $this->convertParams($plugin['params']);
            return $plugin;
        }
    }
}
