<?php
namespace Fivr\Controller;

use Fivr\Model\DatasetModel;
use Fivr\Model\JobModel;
use Fivr\Model\PluginModel;

class Analysis extends Controller
{
    public function index()
    {
        $datasetModel = new DatasetModel();
        $pluginModel = new PluginModel();
        $datasets = $datasetModel->getAllDatasets();
        $plugins = $pluginModel->getAnalysisPlugins();

        if (count($datasets) > 0)
        {

            $options = $datasetModel->getAllVideosAndDatasets();
            $this->render('analysis.index', array('inputs' => $options, 'plugins' => $plugins));
        }
        else
        {
            $this->render('analysis.nodatasets');
        }
    }

    public function analyzeDataset()
    {
        $params = $this->app->request->post();

        if(!isset($params['plugins']))
        {
            $this->app->flash('error', 'Please select at least 1 plugin');
            $this->index();
        }
        else{
            $calls = [];
            foreach($params['plugins'] as $plugin => $value) {
                $arguments = $params[str_replace(".", "_", $plugin)];
                $calls[$plugin] = $arguments;
            }
            $this->addJobs($calls, $params['input']);
        }
    }

    private function addJobs($calls, $input)
    {
        if (substr_count($input, "/") > 1)
        {
            $this->addAnalyzeJob($calls, $input);
        }
        else
        {
            $datasetModel = new DatasetModel();
            $parts = explode("/", $input);
            $encoding = $parts[1];
            $datasetId = $parts[0];
            $dataset = $datasetModel->getDataset($datasetId);
			$ts = time();
            foreach($dataset['videos'] as $video)
            {
                $this->addAnalyzeJob($calls, $datasetId . '/' . $video . '/encodings/' . $encoding, $ts, $datasetId);
            }
        }
        $this->app->redirect('/jobs');
    }

    private function addAnalyzeJob($calls, $input, $ts, $dataset)
    {
        $pluginModel = new PluginModel();
        $jobModel = new JobModel();


        foreach($calls as $pluginId => $params)
        {
            $plugin = $pluginModel->getPlugin($pluginId);
            foreach($plugin['params'] as $param)
            {
                if ($param['typeName'] == "File/Path")
                {
                    $params[$param['name']] = $input;
                }
            }

			$overallJob = $ts."_auto_".$plugin['name']."_".$dataset;
           
            $jobModel->addJob($pluginId, $params, dirname(dirname($input)) . "/" . $pluginId, $overallJob);
        }

    }

}
