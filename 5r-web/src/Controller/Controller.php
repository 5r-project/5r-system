<?php

namespace Fivr\Controller {

    class Controller
    {
        protected $app;

        public function __construct()
        {
            $this->app = \Slim\Slim::getInstance();
        }

        public function render($view, $params = array(), $status = NULL)
        {

            if (! isset($params['autoload']))
                $params['autoload'] = FALSE;

            $params['app'] = $this->app;
            $this->app->render($view, $params, $status);
        }

    }
}
