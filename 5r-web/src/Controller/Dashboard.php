<?php
namespace Fivr\Controller
{

    use Fivr\Model\JobModel;
    use \Predis;
    class Dashboard extends Controller
    {
        public function index()
        {
            $jobModel = new JobModel();
            $redis = new Predis\Client();
            $params = [
                'cnt_worker' => count($redis->keys("5r:worker:*")),
                'cnt_dataset' => count($redis->zrange("5r:datasets", 0, -1)),
                'cnt_job' => $redis->llen('5r:queue:processing')
            ];

            $this->render('dashboard', $params);
        }
    }
}

