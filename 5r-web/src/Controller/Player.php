<?php
namespace Fivr\Controller;


use Fivr\Model\DatasetModel;

class Player extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->datasetModel = new DatasetModel();
    }

    public function index()
    {
        $options = $this->datasetModel->getAllVideos();
        $this->render('player.index', ['options' => $options]);
    }

    public function show($dataset, $video)
    {
        $video = $this->datasetModel->getVideo($dataset . '/' . $video);
        $this->render('player.player', $video);
    }
}
