<?php
namespace Fivr\Controller;


use Fivr\Model\JobModel;

class Job extends Controller
{
    private $jobModel;

    public function __construct()
    {
        parent::__construct();
        $this->jobModel = new JobModel();
    }

    public function index()
    {
        $todo = $this->jobModel->getQueueJobs('todo');
        $processing = $this->jobModel->getQueueJobs('processing');
        $done = array_reverse($this->jobModel->getQueueJobs('done'));
        $this->render('jobs', array('todo' => $todo, 'processing' => $processing, 'done' => $done, 'autoload' => TRUE));
    }

    public function clearDone()
    {
        $this->jobModel->clearQueue('done');
        $this->app->redirect('/jobs');
    }

    public function clearEnqueued()
    {
        $this->jobModel->clearQueue('todo');
        $this->app->redirect('/jobs');
    }

    public function abortAll()
    {
        $this->jobModel->abortAllJobs();
        $this->jobModel->clearJobs();
        $this->app->redirect('/jobs');
    }

    public function abortJob($id) {
        $this->jobModel->abortJob($id);
        $this->app->redirect('/jobs');
    }

	public function stats() {
		$this->render('job_stats', array('groups' => $this->calcStats(), 'autoload' => TRUE));
	}

	public function statsCSV() {
		$groups = $this->calcStats();

		header('Content-Type: text/plain');

		foreach($groups as $kgroup => $vgroup) {
			echo($kgroup.";".$vgroup['start'].";".$vgroup['stop'].";".($vgroup['stop']-$vgroup['start'])/1000.0."\n");
			foreach($vgroup['data'] as $job) {
				echo($job['overallJob']." ".$job['params']."\t".($job['timeStop']-$job['timeStart'])/1000.0."\t".intval($job['timeStart'])."\t".intval($job['timeStop'])."\n");
		
			}
			echo("\n");
		}
		
	}

	public function calcStats() {
		$jobs = $this->jobModel->getAllJobs();

		$start_time = PHP_INT_MAX;
		$stop_time = 0;

		$groups = array();


		//Group by job based on plugin and params
		foreach($jobs as $job) 
		{
			$groups[$job['overallJob']]['data'][] = $job;
		}


		foreach($groups as $kgroup => $vgroup) {
			$start_time = PHP_INT_MAX;
			$stop_time = 0;

			foreach($vgroup['data'] as $job) {
				if (intval($job['timeStart']) < $start_time)
					$start_time = intval($job['timeStart']);
				if (intval($job['timeStop']) > $stop_time)
					$stop_time = intval($job['timeStop']);	
			}

			$groups[$kgroup]['start'] = $start_time;
			$groups[$kgroup]['stop'] = $stop_time;

		}

		return $groups;
	}

    public function create()
    {
        $this->render('job_create');

    }
}
