<?php
namespace Fivr\Controller;

use Fivr\Model\DatasetModel;
use Fivr\Model\JobModel;
use Fivr\Model\PluginModel;

class Dataset extends Controller
{
    protected $datasetModel;

    function __construct()
    {
        parent::__construct();
        $this->datasetModel = new DatasetModel();
    }

    function index()
    {
        $this->render('dataset.index', array('datasets' => $this->datasetModel->getAllDatasets()));
    }

    function import()
    {
        $pluginModel = new PluginModel();

        $importCandidates = $this->datasetModel->getImportCandidates();
        $preprocessors = $pluginModel->getPreprocessingPlugins();
        $this->render('dataset.import', array('candidates' => $importCandidates, 'preprocessors' => $preprocessors));
    }

    function processImport()
    {
        $jobModel = new JobModel();
        $directory = $this->app->request->post('directory');
        $pp = $this->app->request->post('pp');

        $params = array();
        if ($pp != NULL) {
            foreach ($pp as $key => $value) {
                $params[$key] = "true";
            }
        }
        $params['directory'] = $directory;

        $jobModel->addJob('java.DatasetImport', $params, '', '');
        $this->app->redirect('/jobs');
    }

    function view($id)
    {
    	$videos = $this->datasetModel->getVideos($id);
    	//echo json_encode($videos);
    	
        $this->render('dataset.view', array('dataset' => $id, 'videos' => $this->datasetModel->getVideos($id)));
    }

    function delete($id) {
        $this->datasetModel->deleteDataset($id);
        $this->app->redirect('/datasets');
    }
}
