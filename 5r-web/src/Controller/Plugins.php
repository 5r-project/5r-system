<?php

namespace Fivr\Controller
{

    use Fivr\Model\PluginModel;
    use Fivr\Model\WorkerModel;
    use \Predis;
    class Plugins extends Controller
    {
        private $pluginModel;

        function __construct()
        {
            parent::__construct();
            $this->pluginModel = new PluginModel();
        }

        public function index()
        {
            $params = [
                'plugins' => $this->pluginModel->getAllPlugins()
            ];

            $this->render('plugin.index', $params);
        }

        public function clear()
        {
            $this->pluginModel->clearPlugins();
            $this->app->redirect('/plugins');
        }


    }
}

