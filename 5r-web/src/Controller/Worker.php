<?php
namespace Fivr\Controller
{

    use Fivr\Model\PluginModel;
    use Fivr\Model\WorkerModel;
    use \Predis;
    class Worker extends Controller
    {
        private $workerModel;

        function __construct()
        {
            parent::__construct();
            $this->workerModel = new WorkerModel();
        }

        public function index()
        {
            $params = [
                'autoload' => TRUE,
                'workers' => $this->workerModel->getAllWorker(),
            ];

            $this->render('worker', $params);
        }

        public function clearWorker()
        {
            $this->workerModel->clearWorker();
            $this->app->redirect('/worker');
        }
        
        public function restartAllWorker() {
        	$worker = $this->workerModel->getAllWorker();
        	foreach($worker as $w) {
        		$this->workerModel->restartWorker($w['id']);
        	}
            $this->app->redirect('/worker');
        }
        
        public function restartWorker($id) {
        	$this->workerModel->restartWorker($id);
            $this->app->redirect('/worker');
        }


    }
}

