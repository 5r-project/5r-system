@extends('layout.master')

@section('title')
    Analysis
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning">
                No datasets were found to analyze.<br>
                <a href="/datasets/import">Click here</a> to create a new Dataset.
            </div>
        </div>
    </div>
@endsection