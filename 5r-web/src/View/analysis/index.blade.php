@extends('layout.master')

@section('title')
    Analysis
@endsection

@section('css')
    <link href="/assets/css/select2.min.css" rel="stylesheet">
@endsection

@section('content')
<form role="form" method="post" action="/analysis/dataset">

    <div class="row">
            <div class="col-lg-4 col-sm-12">
                <h3>Input Selection</h3>
                <div class="form-group">
                    <label>Select Content</label>
                    <select name="input" class="form-control">
                        @foreach($inputs as $key => $input)
                            <option value="{{$key}}">{{$input}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-success" type="submit">Start Analysis</button>
            </div>
    </div>
    <div class="row">
        <h3>Plug-In configuration</h3>
    </div>
    <div class="row">
        @foreach($plugins as $plugin)
            <div class="col-lg-3 col-md-4 col-sm-12">
            <h4>{{$plugin['name']}}</h4>
                    <div class="form-group">
                        <div class="checkbox">
                            <label><input type="checkbox" class="pluginCheckbox" id="{{$plugin['name']}}" name="plugins[{{$plugin['name']}}]"> <b>Enable</b></label>
                        </div>
                    </div>
                    @include('analysis.params', ['plugin' => $plugin])
            </div>
        @endforeach
    </div>
</form>
@endsection

@section('script')
<script src="/assets/js/select2.min.js"></script>
<script src="/assets/js/analysis.js"></script>
@endsection
