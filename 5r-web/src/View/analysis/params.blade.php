<div class="paramGroup" id="{{$plugin['name']}}.params">
    <div class="form-group">

    @foreach($plugin['params'] as $param)
        <div class="form-group">
            @if($param['typeName'] == "Bool")
                <div class="checkbox">
                    <label>
                        @if($param['value'][0] == "True" || $param['value'][0] == "true" || $param['value'][0] == "1")
                            <input type="checkbox" checked name="{{$plugin['name']}}[{{$param['name']}}]" /> {{$param['name']}}
                        @else
                            <input type="checkbox" name="{{$plugin['name']}}[{{$param['name']}}]" /> {{$param['name']}}
                        @endif
                    </label>
                </div>
            @elseif($param['typeName'] == "Integer" || $param['typeName'] == "Long")
                <label>{{$param['name']}}</label>
                <input type="number" class="form-control" step="1" name="{{$plugin['name']}}[{{$param['name']}}]" value="{{$param['value'][0]}}" />
            @elseif($param['typeName'] == "Double")
                <label>{{$param['name']}}</label>
                <input type="number" step="0.1" class="form-control" name="{{$plugin['name']}}[{{$param['name']}}]" value="{{$param['value'][0]}}" />
            @elseif($param['typeName'] == "String")
                <label>{{$param['name']}}</label>
                <input type="text" class="form-control" name="{{$plugin['name']}}[{{$param['name']}}]" value="{{$param['value'][0]}}" />
            @elseif($param['typeName'] == "Enum")
                <label>{{$param['name']}}</label><br>
                <select name="{{$plugin['name']}}[{{$param['name']}}]">
                    @foreach($param['value'] as $key => $value)
                        @if($key == 0)
                            <option selected="selected" value="{{$value}}">{{$value}}</option>
                        @else
                            <option value="{{$value}}">{{$value}}</option>
                        @endif
                    @endforeach
                </select>
            @endif

        </div>
    @endforeach
    </div>

</div>