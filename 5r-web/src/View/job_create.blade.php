@extends('layout.master')

@section('toolbar')
    @include('job_toolbar')
@endsection

@section('title')
    Create a new Job
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>1: Select Input</h4>
                </div>
                <div class="panel-body">
                    Foo
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>2: Configure Parameter</h4>
                </div>
                <div class="panel-body">
                    Foo
                </div>
            </div>
        </div>

    </div>



@endsection
