@extends('layout.master')
@section('title')
    Worker
@endsection

@section('toolbar')
	<a class="btn btn-info" href="{{$app->urlFor('restartAllWorker')}}" id="btnRestartAllWorker"><i class="glyphicon glyphicon-repeat"></i> Restart all Worker</a>
    <a class="btn btn-warning" href="{{$app->urlFor('clearWorker')}}" id="btnClearWorker"><i class="glyphicon glyphicon-erase"></i> Clear all Worker</a>
@endsection
@section('content')

<div class="row">
    @foreach($workers as $worker)
        <div class="col-lg-3 col-md-4 col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <b>Worker - {{$worker['id']}}</b>
                </div>
                <div class="panel-body">
                    <ul>
                        <li><b>Host: </b> {{$worker['address']}}</li>
                        <li><b>Cores: </b> {{$worker['cores']}}</li>
                        <li><b>Uptime: </b> {{sprintf('%d d %02d:%02d:%02d',$worker['uptime']/86400, $worker['uptime']/3600, $worker['uptime']/60%60, $worker['uptime']%60)}}</li>
                        <li><b>Storage free: </b> {{sprintf("%.1f GB", $worker['discFree']/1024/1024 / 1024)}} of total {{sprintf("%.1f GB", $worker['discTotal']/1024/1024/1024)}} ({{sprintf("%.2f %%", $worker['discFree'] / $worker['discTotal'] * 100.0)}})</li>
                    </ul>
                    <a class="btn btn-info" href="{{$app->urlFor('restartWorker', array('id' => $worker['id']))}}" id="btnRestartAllWorker"><i class="glyphicon glyphicon-repeat"></i> Restart Worker</a>
                </div>
            </div>
        </div>
    @endforeach

</div>
@endsections
