@extends('layout.master')

@section('title')
    Datasets
@endsection

@section('toolbar')
    <li><a class="btn btn-primary" href="{{$app->urlFor('importDataset')}}"><i class="fa fa-plus"> New Dataset</i></a></li>
@endsection
@section('content')
    @if(count($datasets) > 0)
        <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-12">
                @foreach($datasets as $dataset)
                <div class="panel panel-info">
                    <div class="panel-heading">{{$dataset['directory']}}</div>
                    <div class="panel-body">
                        <div class="col-lg-12">
                            <b>Videos: </b> {{count($dataset['videos'])}}
                            <br>
                            <b>Encodings</b>
                            <ul>
                                @foreach($dataset['encodings'] as $encoding)
                                <li>{{$encoding}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="/datasets/view/{{$dataset['id']}}" class="btn btn-info"> View</a>
                        <a href="/datasets/delete/{{$dataset['id']}}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
                @endforeach

        </div>
    @else
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-warning">
                    No datasets available yet. Click <a href="/datasets/import">here</a> to create a new one.
                </div>
            </div>
        </div>

    @endif
    </div>
@endsection