@extends('layout.master')

@section('title')
    Dataset - {{$dataset}}
@endsection

@section('toolbar')
    <li><a class="btn btn-primary" href="{{$app->urlFor('importDataset')}}"><i class="fa fa-plus"> New Dataset</i></a></li>
@endsection
@section('content')
    <div class="row">
        @foreach($videos as $video)
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">{{$video['name']}}</div>
                <div class="panel-body">
                    <b>Encodings: </b>
                    <ul>
                        @foreach($video['encodings'] as $encoding)
                            <li>{{$encoding}}</li>
                        @endforeach
                    </ul>
                    <b>Features: </b>
                    <ul>
                        @foreach($video['features'] as $feature)
                            <li>{{$feature}}</li>
                        @endforeach
                    </ul>
                    <!--<div class="col-lg-12">
                        <b>Files:</b>
                        <ul>
                            @foreach($video['files'] as $path => $file)
                                <li><a href="/{{$path}}">{{$file}}</a></li>
                            @endforeach
                        </ul>
                    </div>-->
                </div>
                <div class="panel-footer">
                    <a class="btn btn-success" href="/player/{{$video['name']}}">View in Player</a>
                </div>
            </div>

        </div>
        @endforeach
    </div>
@endsection
