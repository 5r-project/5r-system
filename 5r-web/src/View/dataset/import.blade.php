@extends('layout.master')

@section('title')
    Import a new Dataset
@endsection

@section('content')
@if(count($candidates) == 0)
    <div class="alert alert-warning">
        <p><b>No data was found to import!</b></p>
        <p>Please create a new folder with the name of the new dataset <code>{{$_SERVER['DOCUMENT_ROOT']."/data"}}</code> containing all the videos for the new dataset to import</p>
        <p>
            <a href="/datasets/import">Try again!</a>
        </p>
    </div>
@else
    <div class="row">
        <div class="col-lg-12">
            <form method="post">
                <div class="form-group">
                    <label for="folder">Import directory</label>
                    <select name="directory" class="form-control">
                        @foreach($candidates as $candidate)
                            <option>{{$candidate}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Preprocessors</label>
                    @foreach($preprocessors as $pp)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="pp[{{$pp['name']}}]">
                                {{$pp['name']}}
                            </label>
                        </div>
                    @endforeach
                </div>
                <button class="btn btn-success" type="submit">Import</button>

            </form>
        </div>
    </div>
@endif
@endsection