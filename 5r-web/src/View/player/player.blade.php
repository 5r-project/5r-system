@extends('layout.master')

@section('title')
    {{$name}}
@endsection

@section('css')
    <link href="/assets/css/player.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <form>
            <input type="hidden" id="videoName" value="{{$name}}">
        </form>

        <div class="col-lg-6">
            <video id="videoPlayer" width="100%" controls>
                <source src="/data/{{$name}}/encodings/{{$encodings[0]}}" type="video/mp4">
                Your Browser does not support the video tag.
            </video>
            <div class="featureVisualisation" id="cxx.MovieBarcode">
                <img id="nordlicht" src="/data/{{$name}}/cxx.MovieBarcode/barcode.png" width="100%" height="100px">
            </div>
            <div class="featureVisualisation" id="cxx.ShotDetection">
                <ul id="shotList" class="shotList">

                </ul>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="form-group">
                @foreach($features as $feature)
                <div class="radio">
                    <label>
                        <input type="radio" value="{{$feature}}" class="featureSelector" name="featureSelector">
                        {{$feature}}
                    </label>

                </div>
                @endforeach
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script src="/assets/js/select2.min.js"></script>
    <script src="/assets/js/player.js"></script>
@endsection