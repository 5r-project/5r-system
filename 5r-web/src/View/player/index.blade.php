@extends('layout.master')

@section('title')
    Video Player
@endsection

@section('css')
    <link href="/assets/css/select2.min.css" rel="stylesheet">
@endsection

@section('content')
    <label>Select Content</label>
    <select name="input" class="form-control">
        @foreach($options as $key => $input)
            <option value="{{$key}}">{{$input}}</option>
        @endforeach
    </select>
@endsection


@section('script')
    <script src="/assets/js/select2.min.js"></script>
    <script src="/assets/js/player.js"></script>
@endsection