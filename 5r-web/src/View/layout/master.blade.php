<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FIVR - @yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/assets/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    @yield('css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/dashboard">Framework for Interactive Video Retrieval</a>
        </div>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="{{$app->urlFor('dashboard')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{$app->urlFor('datasets')}}"><i class="fa fa-database"></i> Datasets</a>
                    </li>
                    <li>
                        <a href="/analysis"><i class="fa fa-line-chart"></i> Analysis</a>
                    </li>
                    <li>
                        <a href="{{$app->urlFor('player')}}"><i class="fa fa-play"></i> Video player</a>
                    </li>
                    <li>
                        <a href="{{$app->urlFor('jobs')}}"><i class="fa fa-tasks"></i> Jobs</a>
                    </li>
					<li>
						<a href="/jobs/stats"><i class="fa fa-area-chart"></i> Statistics</a>
					</li>
                    <li>
                        <a href="{{$app->urlFor('plugins')}}"><i class="fa fa-plug"></i> Plug-Ins</a>
                    </li>
                    <li>
                        <a href="{{$app->urlFor('worker')}}"><i class="fa fa-desktop"></i> Worker</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-pills navbar-right">
					@if($autoload)
						<li><label>Autorefresh <input id="cb_autoload" 		type="checkbox"></li>
					@endif
                    @yield('toolbar')
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">@yield('title')</h3>
                </div>

                <!-- /.col-lg-12 -->
            </div>
            @yield('content')
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="/assets/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/assets/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/assets/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/assets/js/sb-admin-2.js"></script>

@yield('script')

@if($autoload)
    <script src="/assets/js/autoload.js"></script>
@endif
</body>

</html>
