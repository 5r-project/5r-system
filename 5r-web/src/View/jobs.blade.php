@extends('layout.master')

@section('toolbar')
    <li><a href="{{$app->urlFor('clearEnqueued')}}" class="btn btn-primary"><i class="glyphicon glyphicon-erase"></i> Clear Enqueued</a></li>
    <li><a href="{{$app->urlFor('clearDone')}}" class="btn btn-success"><i class="glyphicon glyphicon-erase"></i> Clear Done</a></li>
    <li><a href="{{$app->urlFor('abortAll')}}" class="btn btn-danger"><i class="fa fa-ban"></i> Abort all Jobs</a></li>
@endsection

@section('title')
    Jobs
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-sm-12 col-md-4">
            <h3>Enqueued ({{count($todo)}})</h3>
            @foreach($todo as $job)
            <div class="panel panel-primary">
                <div class="panel-heading">{{$job['id']}} - {{$job['plugin']}}</div>
                <div class="panel-body">
                        {{str_replace(";", "<br>", $job['params'])}}
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-lg-6 col-sm-12 col-md-8">
            <h3>Processing ({{count($processing)}})</h3>
            @foreach($processing as $job)
            <div class="panel panel-yellow">
                <div class="panel-heading">{{$job['id']}} - {{$job['plugin']}}</div>
                <div class="panel-body">
                    <p>
                        {{str_replace(";", "<br>", $job['params'])}}
                    </p>
                    <p><b>Directory: </b> {{$job['directory']}}</p>
                    <p>
                        <b>OutputFiles:</b>
                        {{str_replace(";", ", ", $job['files'])}}
                    </p>
                    <p>
                    	<b>Started: </b>
                    	{{date('d.m.Y H:i:s',$job['timeStart']/1000)}}<br>
                    </p>
                    <p>
                    	<b>Duration: </b>
                    	<?php $duration = (time() - $job['timeStart']/1000); ?>
	                    {{sprintf('%02d:%02d:%02d',$duration/3600, $duration/60%60, $duration%60)}}
                    </p>
                    <p>
                        <b>Info: </b>{{$job['info']}}
                    </p>
                    <p>
                        <a href="/jobs/abortJob/{{$job['id']}}" class="btn btn-danger"><i class="fa fa-ban"></i> Abort</a>
                    </p>
                </div>
                <div class="panel-footer">
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$job['progress']*100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$job['progress']*100}}%">
                            {{number_format($job['progress']*100, 1)}} %
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-lg-3 col-sm-12 col-md-4">
            <h3>Done ({{count($done)}})</h3>
            @foreach($done as $job)
            @if($job['state'] == "FINISHED")
            <div class="panel panel-green">
            @elseif($job['state'] == "ABORTED")
            <div class="panel panel-yellow">
            @else
            <div class="panel panel-red">
            @endif
                <div class="panel-heading">{{$job['id']}} - {{$job['plugin']}}</div>
                <div class="panel-body">
                    <p>
                        {{str_replace(";", "<br>", $job['params'])}}
                    </p>
                    <p><b>Directory: </b> {{$job['directory']}}</p>
                    <p>
                        <b>OutputFiles:</b>
                        {{str_replace(";", "<br>", $job['files'])}}
                    </p>
                    <p>
                        <b>Info: </b>{{$job['info']}}
                    </p>
                    <p>
                    	<b>Schedule: </b>
                    	{{date('d.m.Y H:i:s',$job['timeStart']/1000)}} - {{date('H:i:s',$job['timeStop']/1000)}} <br>
                    </p>
                    <p>
                    	<b>Duration: </b>
                    	<?php $duration = ($job['timeStop'] - $job['timeStart']) / 1000; ?>
	                    {{sprintf('%02d:%02d:%02d',$duration/3600, $duration/60%60, $duration%60)}}
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection
