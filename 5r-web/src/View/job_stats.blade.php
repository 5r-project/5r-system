@extends('layout.master')

@section('title')
    Jobs - Performance Statistics
@endsection

@section('content')
	@foreach($groups as $kgroup => $vgroup)	
	<div class="row">
			<h4>{{$kgroup}}</h4>
			<p>Start: {{date('d.m.Y H:i:s',$vgroup['start']/1000)}}<br>
			Stop: {{date('d.m.Y H:i:s',$vgroup['stop']/1000)}}<br>
			Duration: {{($vgroup['stop'] - $vgroup['start'])/1000.0}}</p>
<pre>
@foreach($vgroup['data'] as $job)
{{$job['params']}}|{{($job['timeStop']-$job['timeStart'])/1000.0}}||{{intval($job['timeStart'])}}||{{intval($job['timeStop'])}}

@endforeach
</pre>
	</div>
	@endforeach
@endsection
