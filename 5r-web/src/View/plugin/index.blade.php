@extends('layout.master')

@section('title')
    Plug-Ins
@endsection

@section('toolbar')
	<li><a href="{{$app->urlFor('clearPlugins')}}" class="btn btn-warning"><i class="glyphicon glyphicon-erase"></i> Clear Plug-Ins</a></li>
@endsection

@section('content')
	<div class="row">
		@foreach($plugins as $plugin)
			<div class="col-lg-3 col-md-4 col-sm-6">
				@if($plugin['type'] == "analysis")
				<div class="panel panel-green">
				@elseif($plugin['type'] == "preprocessing")
				<div class="panel panel-yellow">
				@else
				<div class="panel panel-primary">
				@endif
					<div class="panel-heading"><b>{{$plugin['name']}} - {{$plugin['version']}}</b></div>
				</div>

				<div class="panel-body">
					<b>Type: </b> {{$plugin['type']}}<br>
					<b>Parameters:</b>
					@if(count($plugin['params']) > 0)
						<ul>
							@foreach($plugin['params'] as $param)
								<li>{{$param['name']}} [{{$param['typeName']}}]</li>
							@endforeach
						</ul>
					@else
						None
					@endif
				</div>

			</div>

		@endforeach
	</div>

@endsection
