{{$plugin['name']}}

@foreach($plugin['params'] as $param)
    @if($param['type'] != 6)
        <div class="form-group">
            <label for="{{$plugin['name']}}[{{$param['name']}}]">{{$param['name']}}</label>
            @if($param['type'] == 5)
                <input type="checkbox" name="{{$plugin['name']}}[{{$param['name']}}]">
            @else
                <input type="text" name="{{$plugin['name']}}[{{$param['name']}}]">
            @endif
        </div>
    @endif


@endforeach