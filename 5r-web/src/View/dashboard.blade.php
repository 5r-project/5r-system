@extends('layout.master')

@section('title')
    Dashboard
@endsection

@section('content')
    @if($cnt_worker <= 0)
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-warning">
                    No workers are currently online. Please checked if you started a worker
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-video-camera fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$cnt_dataset}}</div>
                            <div>Datasets</div>
                        </div>
                    </div>
                </div>
                <a href="/datasets">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$cnt_job}}</div>
                            <div>Running Jobs</div>
                        </div>
                    </div>
                </div>
                <a href="{{$app->urlFor('jobs')}}">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-desktop fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$cnt_worker}}</div>
                            <div>Online Worker nodes</div>
                        </div>
                    </div>
                </div>
                <a href="{{$app->urlFor('worker')}}">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection