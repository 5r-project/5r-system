#!/usr/bin/python3
import stat

__author__ = "Peter Spiess-Knafl"
__copyright__ = "Copyright 2015"
__license__ = "GPLv3"
__version__ = "1.0"
__email__ = "dev@spiessknafl.at"
__status__ = "Development"

import argparse
import os
import pkg_resources
import os.path

parser = argparse.ArgumentParser(description="Create native Plug-Ins for the 5r-project")
parser.add_argument('name', help="The name of your Plug-In in camel case")


def create_file_from_template(template, target, class_name):
	fileContent = ""
	writeToFile = True
	with open(pkg_resources.resource_filename(__name__, os.path.join('templates', template)), "rt") as infile:
		for line in infile:
			line = line.replace("{PLUGIN}", class_name.upper())
			line = line.replace("{plugin}", class_name.lower())
			line = line.replace("{Plugin}", class_name)
			fileContent = fileContent + line
			
	if (os.path.isfile(target)):
		compareFile = open(target,"rt")
		compareText = compareFile.read()
		compareFile.close()
		if (compareText == fileContent):
			writeToFile = False
			
	if (writeToFile):
		with open(target, "wt") as outfile:
			outfile.write(fileContent)

def main():

    args = parser.parse_args()

    class_name = args.name

    target_path = class_name
    if not os.path.exists(target_path):
        os.makedirs(os.path.join(target_path, 'interface'))
        os.makedirs(os.path.join(target_path, 'cmake'))

    create_file_from_template('plugin.h', os.path.join(target_path, 'interface', 'plugin.h'), class_name)
    create_file_from_template('plugin.cpp', os.path.join(target_path, 'interface', 'plugin.cpp'), class_name)
    create_file_from_template('IFivrNativePlugin.h', os.path.join(target_path, 'interface', 'IFivrNativePlugin.h'), class_name)
    create_file_from_template('FivrNativePlugin.h', os.path.join(target_path, 'interface', 'FivrNativePlugin.h'), class_name)
    create_file_from_template('FivrNativePlugin.cpp', os.path.join(target_path, 'interface', 'FivrNativePlugin.cpp'), class_name)
    create_file_from_template('Plugin.java', os.path.join(target_path, 'interface', (class_name + '.java')), class_name)
    create_file_from_template('FindJsoncpp.cmake', os.path.join(target_path, 'cmake', 'FindJsoncpp.cmake'), class_name)

    plugin_header = os.path.join(target_path, class_name + ".h")
    plugin_source = os.path.join(target_path, class_name + ".cpp")
    cmake_txt = os.path.join(target_path, 'CMakeLists.txt')
    test_app = os.path.join(target_path, class_name.lower() + '.c')

    if not os.path.isfile(cmake_txt):
        create_file_from_template('CMakeLists.txt', os.path.join(target_path, 'CMakeLists.txt'), class_name)
    else:
        print("Skipping " + cmake_txt + ", because it already exists")
        
    if not os.path.isfile(test_app):
        create_file_from_template('testapplication.c', test_app, class_name)
    else:
        print("Skipping " + test_app + ", because it already exists")

    if not os.path.isfile(plugin_header):
        create_file_from_template('PluginClass.h', plugin_header, class_name)
    else:
        print("Skipping " + plugin_header + ", because it already exists")
    if not os.path.isfile(plugin_source):
        create_file_from_template('PluginClass.cpp', plugin_source, class_name)
    else:
        print("Skipping " + plugin_source + ", because it already exists")

    build_path = os.path.join(target_path, 'build.sh')
    create_file_from_template('build.sh', build_path, class_name)

    #make build script executable
    st = os.stat(build_path)
    os.chmod(build_path, st.st_mode | stat.S_IEXEC)
    
    print("Your plulgin files have been created to: " + target_path)
    print("You can now do the following: ")
    print("    - Open the "+target_path+"/CMakeLists.txt in your favorite IDE") 
    print("    - execute "+target_path+"/build.sh to just build the plugin")

if __name__ == "__main__":
    main()
