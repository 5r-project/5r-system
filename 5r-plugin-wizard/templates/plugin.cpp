/**
  * This file has been generated using the 5r-plugin-wizard
  * Please DO NOT MODIFY this file manually.
  */
#include "plugin.h"
#include "../{Plugin}.h"

#define AS_TYPE(Type, Obj) reinterpret_cast<Type *>(Obj)
#define this AS_TYPE ({Plugin}, context)

using namespace std;
using namespace fivr;

FUNCTION_NAME(fivr_context_t*, context_new, const char* workingDirectory)
{
    {Plugin}* plugin = new {Plugin}(workingDirectory);
    return AS_TYPE(fivr_context_t, plugin);
}

FUNCTION_NAME(void, context_delete, fivr_context_t* context)
{
    if(context)
    {
        delete AS_TYPE({Plugin}, context);
    }
}

FUNCTION_NAME(const char*, get_version, fivr_context_t* context)
{
    return {Plugin}::GetVersion();
}

FUNCTION_NAME(bool, start, fivr_context_t* context)
{
    return this->Start();
}

FUNCTION_NAME(void, stop, fivr_context_t* context)
{
    return this->Stop();
}

FUNCTION_NAME(int, get_state, fivr_context_t* context)
{
    return this->GetState();
}

FUNCTION_NAME(double, get_progress, fivr_context_t* context)
{
    return this->GetProgress();
}

FUNCTION_NAME(const char*, get_generated_files, fivr_context_t* context)
{
    return this->GetProducedFiles();
}

FUNCTION_NAME(const char*, get_config_keys, fivr_context_t* context)
{
    return this->GetConfigKeysString().c_str();
}

FUNCTION_NAME(const char*, get_config, fivr_context_t* context, const char* key)
{
    return this->GetConfig(key).c_str();
}

FUNCTION_NAME(bool, set_config, fivr_context_t* context, const char* key, const char* value)
{
    return this->SetConfig(key, value);
}

FUNCTION_NAME(const char*, get_info, fivr_context_t* context)
{
    return this->GetInfo();
}
