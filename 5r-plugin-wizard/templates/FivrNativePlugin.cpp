/**
  * This file has been generated using the 5r-plugin-wizard
  * Please DO NOT MODIFY this file manually.
  */

#include "FivrNativePlugin.h"

using namespace std;
using namespace fivr;

FivrNativePlugin::FivrNativePlugin(const char *workingDirectory) :
    m_workingDirectory(workingDirectory),
    m_thread(NULL),
    m_run(false),
    m_state(STATE_INIT),
    m_progress(0.0)
{
}

FivrNativePlugin::~FivrNativePlugin()
{
}

bool FivrNativePlugin::Start()
{
    m_progress = 0.0;
    m_producedFile = false;
    m_producedFiles.clear();
    if (!this->Init())
    {
        m_state = STATE_ERROR;
        return false;
    }
    m_run = true;
    m_state = STATE_PROCESSING;
    m_thread = new std::thread(&FivrNativePlugin::ThreadLoop, this);
    return true;
}

void FivrNativePlugin::Stop()
{
    if (m_thread != NULL && m_run)
    {
        m_state = STATE_FINISHED;
        m_run = false;
        m_thread->join();
        delete m_thread;
        m_thread = NULL;
    }
}

State FivrNativePlugin::GetState()
{
    return m_state;
}

bool FivrNativePlugin::SetConfig(const std::string &key, const std::string &value)
{
    if (!ContainsConfigKey(key))
        return false;

    m_configkeys[key].value = value;

    return UpdateConfigKey(key, value);

}

string FivrNativePlugin::GetConfig(const std::string &key)
{
    if (!ContainsConfigKey(key))
        return "";

    return m_configkeys[key].value;
}

map<string, ConfigKey> &FivrNativePlugin::GetConfigKeys()
{
    return m_configkeys;
}

string& FivrNativePlugin::GetConfigKeysString()
{
    return m_configkeyString;
}

const char *FivrNativePlugin::GetProducedFiles()
{
    std::lock_guard<std::mutex> lock(m_filesMutex);
    return m_producedFiles.c_str();
}

double FivrNativePlugin::GetProgress()
{
    std::lock_guard<std::mutex> lock(m_progressMutex);
    return m_progress;
}

bool FivrNativePlugin::ContainsConfigKey(const std::string &key)
{
    return m_configkeys.count(key) > 0;
}

void FivrNativePlugin::AddConfigKey(const char *key, const char *defaultValue, ConfigType type)
{
    if (!ContainsConfigKey(key))
    {
        ConfigKey configKey;
        configKey.key = key;
        configKey.value = defaultValue;
        configKey.type = type;
        m_configkeys[key] = configKey;

        stringstream ss;
        ss << configKey.key;
        ss << ":";
        ss << configKey.type;
        ss << ":";
        ss << configKey.value;
        ss << ";";

        m_configkeyString = m_configkeyString + ss.str();
        this->UpdateConfigKey(key, defaultValue);
    }
}

void FivrNativePlugin::UpdateProgress(double progress)
{
    std::lock_guard<std::mutex> lock(m_progressMutex);
    m_progress = progress;
}

string FivrNativePlugin::GetAbsoluteFilePath(const string &file)
{
    return m_workingDirectory + "/" + file;
}

void FivrNativePlugin::AddOutputFile(const std::string &file)
{
    std::lock_guard<std::mutex> lock(m_filesMutex);
    if (m_producedFile)
        m_producedFiles = m_producedFiles + ";";
    m_producedFiles += file;
    m_producedFile = true;
}

const char* FivrNativePlugin::GetInfo()
{
    std::lock_guard<std::mutex> lock(m_infoMutex);
    return m_info.c_str();
}

void FivrNativePlugin::ThreadLoop()
{
    bool lastProcess = true;
    while (m_run && lastProcess)
        lastProcess = this->Process();

    if (m_progress >= 1.0 && m_run)
        m_state = STATE_FINISHED;
    else
        m_state = STATE_ABORTED;
}

void FivrNativePlugin::SetInfo(const std::string &info)
{
    std::lock_guard<std::mutex> lock(m_infoMutex);
    m_info = info;
}
