#ifndef INC_5R_NATIVE_PLUGIN_{PLUGIN}_H
#define INC_5R_NATIVE_PLUGIN_{PLUGIN}_H


#include "interface/FivrNativePlugin.h"

namespace fivr
{
    class {Plugin} : public FivrNativePlugin
    {
        public:
            {Plugin}(const char* workingDirectory);
            virtual ~{Plugin}();

            static const char* GetVersion();

        protected:
            virtual bool UpdateConfigKey(const std::string &key, const std::string &value);
            virtual bool Init();
            virtual bool Process();

        private:

    };
}


#endif //INC_5R_NATIVE_PLUGIN_{PLUGIN}_H
