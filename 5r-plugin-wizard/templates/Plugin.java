/**
  * This file has been generated using the 5r-plugin-wizard
  * Please DO NOT MODIFY this file manually.
  */
package org.fivr.worker.plugin.cxx;

import org.fivr.worker.plugin.*;
import com.sun.jna.*;

public class {Plugin} extends AbstractNativePlugin {

    private interface CLibrary extends Library {
        CLibrary INSTANCE = (CLibrary)
                Native.loadLibrary("{plugin}", CLibrary.class);

        Pointer {plugin}_context_new(String workingDirectory);
        void {plugin}_context_delete(Pointer context);
        String {plugin}_get_version();

        boolean {plugin}_start(Pointer context);
        boolean {plugin}_stop(Pointer context);
        int {plugin}_get_state(Pointer context);
        double {plugin}_get_progress(Pointer context);

        String {plugin}_get_generated_files(Pointer context);

        String {plugin}_get_config_keys(Pointer context);
        String {plugin}_get_config(Pointer context, String key);
        String {plugin}_get_info(Pointer context);
        boolean {plugin}_set_config(Pointer context, String key, String value);
    }

    private Pointer context;

    public {Plugin}(String workingDirectory) {
        context = CLibrary.INSTANCE.{plugin}_context_new(workingDirectory);
    }

    @Override
    public void dispose() {
        if(context != null)
            CLibrary.INSTANCE.{plugin}_context_delete(context);
        context = null;
    }

    @Override
    public boolean start() {
        return CLibrary.INSTANCE.{plugin}_start(context);
    }

    @Override
    public void stop() {
        CLibrary.INSTANCE.{plugin}_stop(context);
    }

    @Override
    public double getProgress() {
        return CLibrary.INSTANCE.{plugin}_get_progress(context);
    }

    @Override
    public int getStateInt() {
        return CLibrary.INSTANCE.{plugin}_get_state(context);
    }

    @Override
    public String getGeneratedFileString() {
        return CLibrary.INSTANCE.{plugin}_get_generated_files(context);
    }

    @Override
    public  String getConfigKeyString() {
        return CLibrary.INSTANCE.{plugin}_get_config_keys(context);
    }

    @Override
    public String getConfigValue(String key) {
        return CLibrary.INSTANCE.{plugin}_get_config(context, key);
    }

    @Override
    public boolean setConfigValue(String key, String value) {
        return CLibrary.INSTANCE.{plugin}_set_config(context, key, value);
    }

    @Override
    public String getInfo() {
        return CLibrary.INSTANCE.{plugin}_get_info(context);
    }

    @Override
    public String getVersion() {
        return CLibrary.INSTANCE.{plugin}_get_version();
    }

    @Override
    public String getName() {
        return "cxx." + this.getClass().getSimpleName();
    }
}