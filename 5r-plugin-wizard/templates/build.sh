#!/bin/bash

mkdir -p ../{Plugin}-build
cd ../{Plugin}-build
cmake ../{Plugin}
make -j`nproc`
