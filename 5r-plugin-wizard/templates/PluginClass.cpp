#include "{Plugin}.h"

using namespace fivr;
using namespace std;

{Plugin}::{Plugin}(const char* workingDirectory) : FivrNativePlugin(workingDirectory)
{
    //TODO: Add your config keys here
    //this->AddConfigKey("frames", "1000", TYPE_INT);
}

{Plugin}::~{Plugin}()
{
    this->Stop();
}

bool {Plugin}::UpdateConfigKey(const string &key, const string &value)
{
    //TODO: Handle configuration values
    return false;
}

bool {Plugin}::Init()
{
    //TODO: Init + return true
    return false;
}

bool {Plugin}::Process()
{
    //TODO: Process some frames
    return false;
}

const char* {Plugin}::GetVersion()
{
    return "1.0.0";
}
