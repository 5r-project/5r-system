#!/bin/bash
set -e
JNI_TARGET=../5r-worker/src/main/java/org/fivr/worker/plugin/cxx
SO_TARGET=../5r-worker/src/main/resources/linux-x86-64

mkdir -p $JNI_TARGET
mkdir -p $SO_TARGET

for i in $(find ./* -maxdepth 0  -not -name '*-build' -type d -printf '%f\n')
do
	plugin=$(echo $i | tr A-Z a-z)
	../5r-plugin-wizard/5r-plugin-wizard.py $i
	cd $i
	./build.sh
	pwd
	ln -f $(realpath ../$i-build/lib$plugin.so) ../$SO_TARGET/lib$plugin.so
	ln -f $(realpath ../$i/interface/$i.java) ../$JNI_TARGET/$i.java
	cd ..
done
