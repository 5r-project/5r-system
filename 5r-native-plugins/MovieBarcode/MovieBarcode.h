#ifndef INC_5R_NATIVE_PLUGIN_MOVIEBARCODE_H
#define INC_5R_NATIVE_PLUGIN_MOVIEBARCODE_H


#include "interface/FivrNativePlugin.h"
#include <opencv2/highgui/highgui.hpp>

namespace fivr
{
    class MovieBarcode : public FivrNativePlugin
    {
        public:
            MovieBarcode(const char* workingDirectory);
            virtual ~MovieBarcode();

            static const char* GetVersion();

        protected:
            virtual bool UpdateConfigKey(const std::string &key, const std::string &value);
            virtual bool Init();
            virtual bool Process();


        private:
            int i;
            std::string m_input;
            std::string m_output;
            int m_width;
            int m_height;
            std::string m_style;

            cv::VideoCapture m_capture;
            cv::Mat* m_result;
            cv::Mat m_frame;

            bool m_blur;

            long m_column;
            long m_totalFrames;

            long m_improvedWidth;

            int m_columnsPerFrame;
            int m_framesPerColumn;

    };
}


#endif //INC_5R_NATIVE_PLUGIN_MOVIEBARCODE_H
