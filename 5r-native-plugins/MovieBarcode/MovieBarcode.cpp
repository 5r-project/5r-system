#include "MovieBarcode.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <cmath>

using namespace fivr;
using namespace std;
using namespace cv;

MovieBarcode::MovieBarcode(const char* workingDirectory) : FivrNativePlugin(workingDirectory),
    m_result(NULL),
    m_column(0)
{
    this->AddConfigKey("input", "file.mp4", TYPE_FILE);
    this->AddConfigKey("output", "barcode.png", TYPE_STRING);
    this->AddConfigKey("width", "1000", TYPE_INT);
    this->AddConfigKey("height", "300", TYPE_INT);
    this->AddConfigKey("blur", "1", TYPE_BOOL);
    //this->AddConfigKey("style", "horizontal,vertical,middlecolumn", TYPE_ENUM);
}

MovieBarcode::~MovieBarcode()
{
    this->Stop();
    m_capture.release();

    if (m_result != NULL)
        m_result->release();
    delete m_result;
}

bool MovieBarcode::UpdateConfigKey(const string &key, const string &value)
{
    if (key == "input")
        m_input = value;
    else if (key == "output")
        m_output = value;
    else if(key == "width")
        m_width = ConvertString<int>(value);
    else if (key == "height")
        m_height = ConvertString<int>(value);
    //else if(key == "style")
    //    m_style = value;
    else if (key == "blur")
        m_blur = ConvertString<bool>(value);
    else
        return false;
    return true;
}

bool MovieBarcode::Init()
{
    if (!this->m_capture.open(this->m_input))
    {
        this->SetInfo("Could not open file: " + this->m_input);
        return false;
    }


    m_totalFrames = m_capture.get(CV_CAP_PROP_FRAME_COUNT);

    double framesPersColumn = (double)m_totalFrames / (double)m_width;

    m_improvedWidth = ceil((double)m_totalFrames / round(framesPersColumn));



    this->m_result = new cv::Mat(m_height, m_improvedWidth, CV_8UC3);

    if (m_totalFrames < m_width)
    {
        m_columnsPerFrame = m_width / m_totalFrames;
        m_framesPerColumn = 1;
    }
    else
    {
        m_framesPerColumn = round(framesPersColumn);
        m_columnsPerFrame = 1;
    }

    //cout << m_totalFrames << endl;
    //cout << m_improvedWidth << endl;
    //cout << m_framesPerColumn << endl;

    return true;
}

bool MovieBarcode::Process()
{
    //Process some frames
    for (int i = 0; i < m_framesPerColumn; i++)
    {
        if (!m_capture.read(m_frame))
        {
            cv::Mat finalImage(m_height, m_width, CV_8UC3);
            resize(*m_result, finalImage, Size(m_width, m_height));
            if (m_blur)
                GaussianBlur(finalImage, finalImage, Size(23,23), 0, 0);
            cv::imwrite(this->GetAbsoluteFilePath(m_output), finalImage);
            this->AddOutputFile(m_output);
            this->UpdateProgress(1.0);
            return false;
        }
    }


    //cout << "Frame: " << m_column << endl;

    Mat roi(m_frame(Rect(m_frame.cols/2, 0, m_columnsPerFrame, m_frame.rows)));

    resize(roi, (*m_result)(Rect(m_column, 0, m_columnsPerFrame, m_result->rows)), Size(m_columnsPerFrame, m_result->rows));
    m_column += m_columnsPerFrame;

    this->UpdateProgress((double)m_column / (double)m_improvedWidth);
    return true;
}

const char* MovieBarcode::GetVersion()
{
    return "1.0.0";
}
