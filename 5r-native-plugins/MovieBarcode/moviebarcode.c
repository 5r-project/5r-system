#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "interface/plugin.h"

void printHelp(fivr_context_t* ctx) {
    char params[200];
    strcpy(params,moviebarcode_get_config_keys(ctx));
    char* part = strtok(params,":;");
    while (part != NULL) {
        printf("\t--%s ", part);
        part = strtok(NULL,":;");
        part = strtok(NULL, ":;");
        printf("<%s>\n", part);
        part = strtok(NULL, ":;");
    }
}

int parseArguments(fivr_context_t* ctx, int argc, char** argv) {
    fflush(stdout);
    for (int i = 1; i < argc; i+=2) {
        if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i],"--help") == 0) {
            printHelp(ctx);
            moviebarcode_context_delete(ctx);
            exit(0);
        } else if(!moviebarcode_set_config(ctx, argv[i]+2, argv[i+1])) {
            fprintf(stderr, "[Error] setting config key: %s\n", argv[i]+2);
            moviebarcode_context_delete(ctx);
            exit(1);
        }
    }
    return 0;
}

int main(int argc, char** argv) {
		
	fivr_context_t* ctx = moviebarcode_context_new(".");

    printf("moviebarcode %s\n", moviebarcode_get_version(ctx));

    parseArguments(ctx, argc, argv);

	if (! moviebarcode_start(ctx)) {
        fprintf(stderr, "[Error] %s\n", moviebarcode_get_info(ctx));
		moviebarcode_context_delete(ctx);
		return 1;
	}
	
	while(moviebarcode_get_state(ctx) == STATE_PROCESSING) {
        printf("[%.1f %%] Info: %s \r", moviebarcode_get_progress(ctx)*100.0, moviebarcode_get_info(ctx));

        fflush(stdout);
		sleep(1);
	}
	
	printf("Generated Files: %s\n", moviebarcode_get_generated_files(ctx));
	
	moviebarcode_context_delete(ctx);
	return 0;
}
