#!/bin/bash

mkdir -p ../MovieBarcode-build
cd ../MovieBarcode-build
cmake ../MovieBarcode
make -j`nproc`
