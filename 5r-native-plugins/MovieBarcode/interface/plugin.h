/**
  * This file has been generated using the 5r-plugin-wizard
  * Please DO NOT MODIFY this file manually.
  */
#ifndef FIVRPLUGINAPI_MOVIEBARCODE_H
#define FIVRPLUGINAPI_MOVIEBARCODE_H

#ifndef FIVR_API
#  ifdef _WIN32
#     if defined(FIVR_BUILD_SHARED) /* build dll */
#         define FIVR_API __declspec(dllexport)
#     elif !defined(FIVR_BUILD_STATIC) /* use dll */
#         define FIVR_API __declspec(dllimport)
#     else /* static library */
#         define FIVR_API
#     endif
#  else
#     if __GNUC__ >= 4
#         define FIVR_API __attribute__((visibility("default")))
#     else
#         define FIVR_API
#     endif
#  endif
#endif

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    STATE_INIT = 1,
    STATE_PROCESSING = 2,
    STATE_FINISHED = 3,
    STATE_ABORTED = 4,
    STATE_ERROR = 5,
    STATE_INVALID = 0
} State;

typedef struct context fivr_context_t;

#define FUNCTION_NAME(type, name, params...) FIVR_API type moviebarcode_ ## name (params)


FUNCTION_NAME(fivr_context_t*, context_new, const char* workingDirectory);
FUNCTION_NAME(void, context_delete, fivr_context_t* context);
FUNCTION_NAME(const char*, get_version, fivr_context_t* context);

FUNCTION_NAME(bool, start, fivr_context_t* context);
FUNCTION_NAME(void, stop, fivr_context_t* context);
FUNCTION_NAME(int, get_state, fivr_context_t* context);
FUNCTION_NAME(double, get_progress, fivr_context_t* context);

FUNCTION_NAME(const char*, get_generated_files, fivr_context_t* context);

FUNCTION_NAME(const char*, get_config_keys, fivr_context_t* context);
FUNCTION_NAME(const char*, get_config, fivr_context_t* context, const char* key);
FUNCTION_NAME(bool, set_config, fivr_context_t* context, const char* key, const char* value);

FUNCTION_NAME(const char*, get_info, fivr_context_t* context);

#ifdef __cplusplus
}
#endif

#endif // FIVRPLUGINAPI_MOVIEBARCODE_H
