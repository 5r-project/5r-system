#!/bin/bash

mkdir -p ../ShotDetection-build
cd ../ShotDetection-build
cmake ../ShotDetection
make -j`nproc`
