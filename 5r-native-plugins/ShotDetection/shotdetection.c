#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "interface/plugin.h"

void printHelp(fivr_context_t* ctx) {
    char params[200];
    strcpy(params,shotdetection_get_config_keys(ctx));
    char* part = strtok(params,":;");
    while (part != NULL) {
        printf("\t--%s ", part);
        part = strtok(NULL,":;");
        part = strtok(NULL, ":;");
        printf("<%s>\n", part);
        part = strtok(NULL, ":;");
    }
}

int parseArguments(fivr_context_t* ctx, int argc, char** argv) {
    fflush(stdout);
    for (int i = 1; i < argc; i+=2) {
        if (strcmp("-h", argv[i]) == 0 || strcmp("--help", argv[i]) == 0) {
            printHelp(ctx);
            shotdetection_context_delete(ctx);
            exit(1);
        } else if(!shotdetection_set_config(ctx, argv[i]+2, argv[i+1])) {
            fprintf(stderr, "[Error] setting config key: %s\n", argv[i]+2);
            shotdetection_context_delete(ctx);
            exit(1);
        }
    }
    return 0;
}

int main(int argc, char** argv) {
		
	fivr_context_t* ctx = shotdetection_context_new(".");

    printf("shotdetection %s\n", shotdetection_get_version(ctx));

    parseArguments(ctx, argc, argv);

	if (! shotdetection_start(ctx)) {
        fprintf(stderr, "[Error] %s\n", shotdetection_get_info(ctx));
		shotdetection_context_delete(ctx);
		return 1;
	}
	
	while(shotdetection_get_state(ctx) == STATE_PROCESSING) {
		printf("Progress: %f %% \n", shotdetection_get_progress(ctx) * 100.0);
        fflush(stdout);
		sleep(1);
	}
	
	printf("Generated Files: %s\n", shotdetection_get_generated_files(ctx));
	
	shotdetection_context_delete(ctx);
	return 0;
}
