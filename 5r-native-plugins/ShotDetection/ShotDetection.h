#ifndef INC_5R_NATIVE_PLUGIN_SHOTDETECTION_H
#define INC_5R_NATIVE_PLUGIN_SHOTDETECTION_H


#include "interface/FivrNativePlugin.h"
#include <opencv2/highgui/highgui.hpp>
#include <json/json.h>

namespace fivr
{
    class ShotDetection : public FivrNativePlugin
    {
        public:
            ShotDetection(const char* workingDirectory);
            virtual ~ShotDetection();

            static const char* GetVersion();

        protected:
            virtual bool UpdateConfigKey(const std::string &key, const std::string &value);
            virtual bool Init();
            virtual bool Process();

        private:
            cv::VideoCapture capture;
            std::vector<cv::Point2f> prevCorners;
            std::vector<cv::Point2f> corners;
            std::vector<std::string> framesKeyframes;
            std::vector<cv::Mat> frameCache;
            cv::Mat lastAddedFrameGray;
            cv::Mat currentFrameGray;
            cv::Mat prevFrame;

            long count;
            int countDetections;
            int countIgnored;

            bool firstframewritten;
            bool slow;
            long lastDetection;

            int SAMPLEX = -1;
            int SAMPLEY = -1; //set below

            //Parameters
            std::string inputFile;
            bool cloudRemoval;
            bool writeShots;
            double threshold;
            double frameRate;
            bool gui;

            Json::Value index;
            long totalFrames;
    };

}


#endif //INC_5R_NATIVE_PLUGIN_SHOTDETECTION_H
