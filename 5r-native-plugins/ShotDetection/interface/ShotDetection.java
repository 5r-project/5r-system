/**
  * This file has been generated using the 5r-plugin-wizard
  * Please DO NOT MODIFY this file manually.
  */
package org.fivr.worker.plugin.cxx;

import org.fivr.worker.plugin.*;
import com.sun.jna.*;

public class ShotDetection extends AbstractNativePlugin {

    private interface CLibrary extends Library {
        CLibrary INSTANCE = (CLibrary)
                Native.loadLibrary("shotdetection", CLibrary.class);

        Pointer shotdetection_context_new(String workingDirectory);
        void shotdetection_context_delete(Pointer context);
        String shotdetection_get_version();

        boolean shotdetection_start(Pointer context);
        boolean shotdetection_stop(Pointer context);
        int shotdetection_get_state(Pointer context);
        double shotdetection_get_progress(Pointer context);

        String shotdetection_get_generated_files(Pointer context);

        String shotdetection_get_config_keys(Pointer context);
        String shotdetection_get_config(Pointer context, String key);
        String shotdetection_get_info(Pointer context);
        boolean shotdetection_set_config(Pointer context, String key, String value);
    }

    private Pointer context;

    public ShotDetection(String workingDirectory) {
        context = CLibrary.INSTANCE.shotdetection_context_new(workingDirectory);
    }

    @Override
    public void dispose() {
        if(context != null)
            CLibrary.INSTANCE.shotdetection_context_delete(context);
        context = null;
    }

    @Override
    public boolean start() {
        return CLibrary.INSTANCE.shotdetection_start(context);
    }

    @Override
    public void stop() {
        CLibrary.INSTANCE.shotdetection_stop(context);
    }

    @Override
    public double getProgress() {
        return CLibrary.INSTANCE.shotdetection_get_progress(context);
    }

    @Override
    public int getStateInt() {
        return CLibrary.INSTANCE.shotdetection_get_state(context);
    }

    @Override
    public String getGeneratedFileString() {
        return CLibrary.INSTANCE.shotdetection_get_generated_files(context);
    }

    @Override
    public  String getConfigKeyString() {
        return CLibrary.INSTANCE.shotdetection_get_config_keys(context);
    }

    @Override
    public String getConfigValue(String key) {
        return CLibrary.INSTANCE.shotdetection_get_config(context, key);
    }

    @Override
    public boolean setConfigValue(String key, String value) {
        return CLibrary.INSTANCE.shotdetection_set_config(context, key, value);
    }

    @Override
    public String getInfo() {
        return CLibrary.INSTANCE.shotdetection_get_info(context);
    }

    @Override
    public String getVersion() {
        return CLibrary.INSTANCE.shotdetection_get_version();
    }

    @Override
    public String getName() {
        return "cxx." + this.getClass().getSimpleName();
    }
}