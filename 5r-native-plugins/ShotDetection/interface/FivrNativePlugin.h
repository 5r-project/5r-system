/**
  * This file has been generated using the 5r-plugin-wizard
  * Please DO NOT MODIFY this file manually.
  */
#ifndef INC_5R_NATIVE_PLUGIN_FIVRNATIVEPLUGIN_H
#define INC_5R_NATIVE_PLUGIN_FIVRNATIVEPLUGIN_H

#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <mutex>
#include <thread>

#include "IFivrNativePlugin.h"

namespace fivr
{
    typedef enum
    {
        TYPE_STRING = 1,
        TYPE_INT = 2,
        TYPE_LONG = 3,
        TYPE_DOUBLE = 4,
        TYPE_BOOL = 5,
        TYPE_FILE = 6,
        TYPE_ENUM = 7,
        TYPE_INVALID = 0
    } ConfigType;

    typedef enum {
        STATE_INIT = 1,
        STATE_PROCESSING = 2,
        STATE_FINISHED = 3,
        STATE_ABORTED = 4,
        STATE_ERROR = 5,
        STATE_INVALID = 0
    } State;

    typedef struct config_key
    {
            std::string key;
            std::string value;
            ConfigType type;

    } ConfigKey;

    class FivrNativePlugin : public IFivrNativePlugin
    {
        public:
            FivrNativePlugin(const char* workingDirectory);
            virtual ~FivrNativePlugin();

            bool Start();
            void Stop();

            //State + Progress
            State GetState();
            double GetProgress();

            //Configuration
            std::map<std::string, ConfigKey>& GetConfigKeys();
            std::string& GetConfigKeysString();
            bool SetConfig(const std::string &key, const std::string &value);
            std::string GetConfig(const std::string &key);

            const char* GetProducedFiles();

            const char* GetInfo();


        protected:
            /**
             * @brief ContainsConfigKey checks if a configuration key is valid for the current plugin.
             * @param key
             * @return true if the key exists, false otherwhise.
             */
            bool ContainsConfigKey(const std::string &key);

            /**
             * @brief AddConfigKey - call this method within the ctor of your plug-in subclass.
             * @param key
             * @param defaultValue
             * @param type
             */
            void AddConfigKey(const char *key, const char *defaultValue, ConfigType type);

            /**
             * @brief UpdateProgress should be called in each invocation of Process() to updated the progress value.
             * @param progress the current progress.
             */
            void UpdateProgress(double progress);

            /**
             * @brief GetAbsoluteFilePath transforms a relative filename (without path) to a absolute path,
             * based on the working directory passed along with the ctor.
             *
             * This method should be used for all read and written files by the native plugin.
             * E.g. if a config parameter contains a file path, this method should be applied to its value.
             * @param file
             * @return absolute path for file
             */
            std::string GetAbsoluteFilePath(const std::string &file);

            /**
             * @brief AddOutputFile notifies the user of the plugin that there is a new produced output file available.
             * **Important:** Do not use the absolute file path. Only the relative path is valid.
             * @param file or direcotry which has been produced.
             */
            void AddOutputFile(const std::string &file);

            void SetInfo(const std::string &info);

        private:
            std::string m_workingDirectory;
            std::map<std::string, ConfigKey> m_configkeys;
            std::string m_producedFiles;
			std::string m_configkeyString;
            std::thread* m_thread;
            std::mutex m_progressMutex;
            std::mutex m_filesMutex;
            std::mutex m_infoMutex;
            std::string m_info;
            bool m_run;
            State m_state;
            double m_progress;
            bool m_producedFile;

            void ThreadLoop();
    };

    template<typename T>
    static T ConvertString(const std::string& value)
    {
        if (std::is_same<T, bool>::value)
        {
            if (value == "on" || value == "true" || value == "TRUE" || value == "True" || value == "1")
                return true;
            return false;
        }

        T result;
        std::istringstream buffer(value);
        buffer >> result;
        return result;
    }

}
#endif //INC_5R_NATIVE_PLUGIN_FIVRNATIVEPLUGIN_H
