/**
  * This file has been generated using the 5r-plugin-wizard
  * Please DO NOT MODIFY this file manually.
  */
#ifndef IFIVRNATIVEPLUGIN
#define IFIVRNATIVEPLUGIN

namespace fivr
{
    class IFivrNativePlugin {
        public:
            virtual ~IFivrNativePlugin() {}

        protected:
            /**
             * @brief UpdateConfigKey should handle the setting of new configkey values. e.g. initialize a private member variable with the new value.
             * For convenience, there is also the ConvertString() method to convert the string values to other primitive types.
             * @param key - the key of the parameter
             * @param value - the new value of the parameter
             * @return true on success, false if the new config key was invalid.
             */
            virtual bool UpdateConfigKey(const std::string &key, const std::string &value) = 0;

            /**
             * @brief Init should initialize the native plugin. If something goes wrong (return false),
             * the method Process() won't get called.
             * @return true on success, false is something during initialization went wrong.
             */
            virtual bool Init() = 0;

            /**
             * @brief Process gets called in each thread iteration. Usually this method should handle the processing of one frame.
             * @return true if the process should continue, false otherwhise.
             */
            virtual bool Process() = 0;
    };
}

#endif // IFIVRNATIVEPLUGIN

