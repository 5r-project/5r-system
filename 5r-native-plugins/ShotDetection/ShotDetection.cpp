#include "ShotDetection.h"

#include <opencv2/core/core.hpp>
//##include <opencv2/videoio.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <iomanip>

using namespace fivr;
using namespace std;
using namespace cv;

ShotDetection::ShotDetection(const char* workingDirectory) : FivrNativePlugin(workingDirectory)
{
    this->AddConfigKey("input", "file.mp4", TYPE_FILE);
    this->AddConfigKey("threshold", "0.6", TYPE_DOUBLE);
    this->AddConfigKey("cloudremoval", "1", TYPE_BOOL);
    this->AddConfigKey("gui", "0", TYPE_BOOL);
    this->AddConfigKey("writeShots", "0", TYPE_BOOL);
}

ShotDetection::~ShotDetection()
{
    this->Stop();
    this->capture.release();
}

bool ShotDetection::UpdateConfigKey(const string &key, const string &value)
{
    if (key == "input")
        this->inputFile = value;
    else if (key == "threshold")
        this->threshold = ConvertString<double>(value);
    else if (key == "cloudremoval")
        this->cloudRemoval = ConvertString<bool>(value);
    else if (key == "gui")
        this->gui = ConvertString<bool>(value);
    else if (key == "writeShots")
        this->writeShots = ConvertString<bool>(value);
    else
        return false;
    return true;
}

bool ShotDetection::Init()
{
    count = 0;
    countDetections = 0;
    countIgnored = 0;
    lastDetection = 0;
    firstframewritten = false;
    slow = false;

    this->capture.open(this->inputFile);
    totalFrames = this->capture.get(CV_CAP_PROP_FRAME_COUNT);
    frameRate = this->capture.get(CV_CAP_PROP_FPS);

    if(!this->capture.isOpened())
    {
        this->SetInfo("Could not open file: " + this->inputFile);
    }
    return this->capture.isOpened();
}

double checkBlurryness(cv::Mat frame)
{
    //the bigger, the less blurred

    cv::Mat frameGray, resized;
    cv::resize(frame, resized, Size(640,360));
    cv::cvtColor(resized, frameGray, CV_RGB2GRAY);

    cv::Mat g1, g2, result;
    cv::GaussianBlur(frameGray, g1, Size(3,3), 0);
    cv::GaussianBlur(frameGray, g2, Size(7,7), 0);
    result = g1 - g2;

    int pcount=0,x,y ;
    double sum = 0;
    for (y=result.rows/4.0; y < result.rows * 3 / 4.0; y++)
        for (x=result.cols/4.0; x < result.cols * 3 / 4.0; x++,pcount++)
            sum += result.at<uchar>(y,x);
    sum /= pcount;

    return sum;
}

bool ShotDetection::Process()
{
    int WIDTH = 320*2, HEIGHT = 176*2;

    corners.clear();
    Mat frame;
    Mat tmpFrame;
    if (!capture.read(tmpFrame))
    {
        index["detections"] = countDetections;
        index["ignored"] = countIgnored;

        //Write index

        ofstream indexFile;
        indexFile.open(GetAbsoluteFilePath("index.json"));
        indexFile << index.toStyledString();
        indexFile.close();
        this->AddOutputFile("index.json");
        this->UpdateProgress(1.0);
        return false;
    }
    count++;

    float aspect = (float)tmpFrame.cols / tmpFrame.rows;
    HEIGHT = (int)(WIDTH / aspect);

    //resize to 640*x
    cv::resize(tmpFrame, frame, Size(WIDTH, HEIGHT));

    frameCache.push_back(frame.clone());
    if (frameCache.size() > 25) {
        frameCache.erase(frameCache.begin());
    }

    if (count == 1)
    {
        //dense sampling
        SAMPLEX = frame.cols / 32;
        SAMPLEY = frame.rows / 32;
        corners.clear();
        for (int y=SAMPLEY; y < frame.rows - SAMPLEY; y+=SAMPLEY)
        {
            for (int x=SAMPLEX; x < frame.cols - SAMPLEX; x+=SAMPLEX) {
                corners.push_back(Point2f(x,y));
            }
        }

    }

    if (count > 1)
    {
        vector<uchar> status;
        vector<float> error;


        calcOpticalFlowPyrLK(prevFrame, frame, prevCorners, corners, status, error);


        int numPointsFound = 0, numPointsNotFound = 0, disabledPoints = 0;


        for( int i=0; i < status.size(); i++ ){
            Point2f pointI = corners[i];
            if (status[i] != 0) {

                if (count % 10 == 0 && cloudRemoval) {
                    //on every 10th frame check for to close points
                    int neighbors=0;
                    for (int j=0; j < corners.size(); j++) {
                        if (i != j) {
                            Point2f pointJ = corners[j];
                            if (fabs(pointI.x - pointJ.x) < SAMPLEX && fabs(pointI.y - pointJ.y) < SAMPLEY)
                                neighbors++;
                        }
                    }

                    if (neighbors >= 10) { //more than 10 points
                        status[i] = 0;
                        numPointsNotFound++;
                        //disable point
                        corners[i].x = frame.cols*10;
                        corners[i].y = frame.rows*10;
                        disabledPoints++;
                    }
                    else
                        numPointsFound++;
                }
                else
                    numPointsFound++;

            } else {
                numPointsNotFound++;
                //disable point
                corners[i].x = frame.cols*10;
                corners[i].y = frame.rows*10;
                disabledPoints++;
            }
        }


        if ((float)numPointsNotFound / (numPointsNotFound + numPointsFound) >= threshold || !firstframewritten)
        {

            if (!firstframewritten || count - lastDetection > 25)  //maximum every 1 seconds a keyframe
            {

                //find least blurry frame in current two seconds
                int RANGE = 5 + frameCache.size(); //one second in future (and one in past through frameCache)

                Mat framei[RANGE];
                double blurryness[RANGE];

                int j;
                for (j=0; j < frameCache.size(); j++) {
                    framei[j] = frameCache.at(j);
                    blurryness[j] = checkBlurryness(framei[j]); //just take current border
                }
                for (; j < RANGE; j++) {
                    Mat tmpFrame;
                    if (!capture.read(tmpFrame))
                        break;

                    //resize to 320x176
                    cv::resize(tmpFrame, framei[j], Size(WIDTH, HEIGHT));

                    blurryness[j] = checkBlurryness(tmpFrame);
                }


                int minIdx = 0;
                double maxNonBlurry = blurryness[0];
                for (int jj=0; jj < j; jj++) {
                    if (blurryness[jj] > maxNonBlurry) {
                        maxNonBlurry = blurryness[jj];
                        minIdx = jj;
                    }
                }

                int frameno = count + (minIdx - frameCache.size());
                //check if not too blurry, otherwise ignore
                //if (maxNonBlurry >= BLURRY)
                {
                    bool ignore(false);

                    {
                        Json::Value entry;
                        if (this->writeShots)
                        {
                            ostringstream imgname;
                            imgname << "shot_" << frameno << ".jpg";
                            entry["image"] = imgname.str();
                            framesKeyframes.push_back(imgname.str());
                            string path = GetAbsoluteFilePath(imgname.str());
                            imwrite(path, framei[minIdx]);
                            this->AddOutputFile(path);




                        }
                        countDetections++;
                        firstframewritten = true;
                        entry["frame"] = frameno;
                        entry["time"] = (double) frameno / frameRate;
                        index["shots"].append(entry);

                    }
                }

                //sample dense again (clean restart)
                corners.clear();
                for (int y=SAMPLEY; y < frame.rows - SAMPLEY; y+=SAMPLEY)
                {
                    for (int x=SAMPLEX; x < frame.cols - SAMPLEX; x+=SAMPLEX) {
                        corners.push_back(Point2f(x,y));
                    }
                }


                //update position of last detection
                lastDetection = count + minIdx;

                //update count
                count += RANGE - frameCache.size();

                //clear frameCache
                frameCache.clear();
            }
        }

        //visualization
        if (gui)
        {
            Mat frameCopy = frame.clone();
            for( int i=0; i < status.size(); i++ ){
                if (status[i] != 0) {
                    Point p0( ceil( prevCorners[i].x ), ceil( prevCorners[i].y ) );
                    Point p1( ceil( corners[i].x ), ceil( corners[i].y ) );

                    line( frameCopy, p0, p1, cv::Scalar(0,255,255,0), 2);
                }
            }

            Mat resized;
            cv::resize(frameCopy, resized, Size(frame.cols,frame.rows));

            cv::imshow("Frame", resized);
        }

        prevCorners.clear();
    }

    prevFrame = frame.clone();
    prevCorners = vector<Point2f>(corners);

    if (gui)
    {
        int wait = cv::waitKey(1);

        if (wait == (int)'p')
        {
            cv::waitKey(0);
        }
        else if (wait >= 0)
            slow = !slow;

        if (slow)
            usleep(100000);
    }

    this->UpdateProgress(count / (double)totalFrames);
    return true;
}

const char* ShotDetection::GetVersion()
{
    return "1.0.0";
}
