package org.fivr.worker;

import org.fivr.worker.job.RedisQueue;
import redis.clients.jedis.JedisPool;

/**
 * Created by Peter Spiess-Knafl on 12/9/15.
 */
public class WorkerTest {

    private Worker worker;
    private WorkerSettings settings;
    private JedisPool pool;
    private RedisQueue queue;

    @org.junit.Before
    public void setUp() {
        settings = WorkerSettings.getInstance();
        worker = new Worker(settings);
        worker.setup();
        pool = new JedisPool(settings.getRedisHost(), settings.getRedisPort());
        queue = new RedisQueue(pool);
    }

    @org.junit.After
    public void tearDown() {
    }

    /*@Test
    public void testImport() throws InterruptedException {
        WorkerJob j = new WorkerJob("java.DatasetImport", "directory", "dataset2", "java.Transcode480p", "true");
        j.setOutputDirectory("");
        String id = queue.addJob(j);

        do {
            j = queue.getJob(id);
            Thread.sleep(100);
        } while(j.getState() != PluginState.STATE_FINISHED);

    }*/



}
