package org.fivr.worker.job;

import org.fivr.worker.plugin.AbstractJavaPlugin;

/**
 * Created by Peter Spiess-Knafl on 12/9/15.
 */
public class MockPlugin extends AbstractJavaPlugin {

    public MockPlugin(String workingDirectory) {
        super("0.0.1", workingDirectory);
    }

    @Override
    protected boolean init() {
        return true;
    }

    @Override
    public double getProgress() {
        return 0;
    }

    @Override
    public boolean setConfigValue(String key, String value) {
        return false;
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public void run() {

    }
}
