package org.fivr.worker.job;

import org.fivr.worker.WorkerSettings;
import org.fivr.worker.plugin.IPluginManager;
import org.fivr.worker.plugin.PluginState;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.JedisPool;

import static org.junit.Assert.assertEquals;

public class JobSchedulerTest {

    private IPluginManager pluginManager;
    private MockJobQueue jobQueue;
    private JedisPool pool;
    private JobScheduler scheduler;

    @Before
    public void setUp() throws Exception {
        pluginManager = new MockPluginManager();
        jobQueue = new MockJobQueue();
        scheduler = new JobScheduler(pluginManager, jobQueue, WorkerSettings.getInstance().getDataPath(), 1);
    }

    /*@Test
    public void testInvalidPlugin() throws Exception {
        WorkerJob j = new WorkerJob("invalidplugin");

        jobQueue.setJob(j);

        scheduler.scheduleJob();
        scheduler.updateJobs();

        assertEquals(PluginState.STATE_ERROR, jobQueue.getJob().getState());
        assertEquals("Could not find plugin with ID: invalidplugin", jobQueue.getJob().getInfo());
    }*/

    @Test
    public void testValidPlugin() throws Exception {
        WorkerJob j = new WorkerJob("mockplugin");
        jobQueue.setJob(j);

        scheduler.scheduleJob();
        scheduler.updateJobs();

        assertEquals(PluginState.STATE_PROCESSING, jobQueue.getJob().getState());
    }
}