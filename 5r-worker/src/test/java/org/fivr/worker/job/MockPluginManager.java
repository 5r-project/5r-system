package org.fivr.worker.job;

import org.fivr.worker.plugin.IWorkerPlugin;

import java.util.List;
import java.util.Map;

/**
 * Created by Peter Spiess-Knafl on 12/9/15.
 */
public class MockPluginManager implements org.fivr.worker.plugin.IPluginManager {
    @Override
    public IWorkerPlugin getPluginWorker(String plugin, String workingDirectory, Map<String, String> params) {
        if(plugin.equals("mockplugin"))
            return new MockPlugin(workingDirectory);
        return null;
    }

    @Override
    public void releasePluginWorker(IWorkerPlugin plugin) {
        plugin.dispose();
    }

    @Override
    public List<IWorkerPlugin> getAvailablePlugins() {
        return null;
    }

    @Override
    public void dispose() {

    }
}
