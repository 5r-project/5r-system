package org.fivr.worker.job;

import org.fivr.worker.plugin.PluginState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.embedded.RedisServer;

import java.util.HashMap;

import static org.junit.Assert.*;

public class RedisQueueTest {

    public static final int PORT = 6378;
    private RedisServer redisServer;
    private RedisQueue queue;
    private JedisPool pool;
    private Jedis client;

    @Before
    public void setUp() throws Exception {
        redisServer = new RedisServer(PORT);
        redisServer.start();
        pool = new JedisPool("localhost", PORT);
        queue = new RedisQueue(pool);
        client = pool.getResource();
    }

    @After
    public void tearDown() throws Exception {
        redisServer.stop();
    }

    @Test
    public void testGetWaitingJob() throws Exception {
        WorkerJob job = queue.getWaitingJob(1);
        assertNull(job);

        queue.addJob(getDefaultRedisJob());
        job = queue.getWaitingJob(0);
        assertEquals("1", job.getId());
        assertEquals("someplugin", job.getPlugin());
        assertEquals("value1",job.getParams().get("param1"));
        assertEquals("value2",job.getParams().get("param2"));
    }

    @Test
    public void testAddJob() throws Exception {
        WorkerJob job = getDefaultRedisJob();
        queue.addJob(job);
        assertEquals("1", client.hget(RedisQueue.JOB_KEY_PREFIX + "1", WorkerJob.ID));
        assertEquals("someplugin", client.hget(RedisQueue.JOB_KEY_PREFIX + "1", WorkerJob.PLUGIN));
        assertEquals("param1:value1;param2:value2", client.hget(RedisQueue.JOB_KEY_PREFIX + "1", WorkerJob.PARAMS));
    }

    private WorkerJob getDefaultRedisJob() {
        HashMap<String, String> params = new HashMap<>();
        params.put("param1", "value1");
        params.put("param2", "value2");
        return new WorkerJob("someplugin", params);
    }

    @Test
    public void testUpdateJob() throws Exception {
        WorkerJob job = getDefaultRedisJob();
        queue.addJob(job);
        job.setProgress(0.3);
        job.setState(PluginState.STATE_PROCESSING);
        queue.updateJob(job);

        assertEquals("0.3", client.hget(RedisQueue.JOB_KEY_PREFIX + job.getId(), WorkerJob.PROGRESS));
        assertEquals("PROCESSING", client.hget(RedisQueue.JOB_KEY_PREFIX + job.getId(), WorkerJob.STATE));
    }

    @Test
    public void testFinishJob() throws Exception {
        WorkerJob job = getDefaultRedisJob();
        queue.addJob(job);
        job = queue.getWaitingJob(0);
        queue.finishJob(job);
        //assertEquals("FINISHED", client.hget(RedisQueue.JOB_KEY_PREFIX + job.getId(), WorkerJob.STATE));
    }

    @Test
    public void testRemoveJob() throws Exception {
        WorkerJob job = getDefaultRedisJob();
        queue.addJob(job);
        job = queue.getWaitingJob(0);
        assertNotNull(job);
        queue.removeJob(job);
        assertNull(client.hget(RedisQueue.JOB_KEY_PREFIX + job.getId(), WorkerJob.STATE));
        assertNull(client.hget(RedisQueue.JOB_KEY_PREFIX + job.getId(), WorkerJob.PROGRESS));
        assertNull(client.hget(RedisQueue.JOB_KEY_PREFIX + job.getId(), WorkerJob.ID));
        assertNull(client.hget(RedisQueue.JOB_KEY_PREFIX + job.getId(), WorkerJob.PLUGIN));

    }
}