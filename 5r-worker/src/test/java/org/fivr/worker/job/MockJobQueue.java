package org.fivr.worker.job;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Peter Spiess-Knafl on 12/9/15.
 */
public class MockJobQueue implements IJobQueue {


    private WorkerJob job;

    public WorkerJob getJob() {
        return job;
    }

    public void setJob(WorkerJob job) {
        this.job = job;
    }

    @Override
    public void updateJob(WorkerJob job) {
        this.job = job;
    }

    @Override
    public void finishJob(WorkerJob job) {
        this.job = job;
    }

    @Override
    public WorkerJob getWaitingJob(int timeout) {
        if (this.job != null) {
            WorkerJob job = this.job;
            this.job = null;
            return job;
        }
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Set<String> getAbortJobs() {
        return new HashSet<>();
    }

    @Override
    public void removeAbortJobs(Set<String> jobs) {

    }

}
