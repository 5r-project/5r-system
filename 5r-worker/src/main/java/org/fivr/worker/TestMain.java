package org.fivr.worker;

import org.fivr.worker.plugin.PluginState;
import org.fivr.worker.plugin.cxx.MovieBarcode;

import java.io.File;
import java.util.ArrayList;


public class TestMain {

    public static void main(String[] args) throws InterruptedException {


        ArrayList<Thread> threads = new ArrayList<>();

        for (int i=0; i < 1; i++) {

            final int finalI = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    String targetPath = "/home/cinemast/Downloads/output_" + String.valueOf(finalI);
                    new File(targetPath).mkdirs();
                    MovieBarcode plugin = new MovieBarcode(targetPath);
                    plugin.setConfigValue("input", "/home/cinemast/git/gitlab/5r-project/fivr-web/data/itec-homevids/Video10_Aquarium_1/encodings/original.mp4");
                    plugin.setConfigValue("style", "horizontal");

                    plugin.start();

                    while (plugin.getState() != PluginState.STATE_FINISHED) {
                        System.out.println("Progress["+finalI+"]: "  + plugin.getProgress() * 100 + " %");
                        System.out.println(plugin.getGeneratedFileString());
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    System.out.println(plugin.getGeneratedFiles());

                    plugin.dispose();
                }
            });
            t.start();
            threads.add(t);
        }

        for (Thread t : threads) {
            t.join();
        }


        System.out.println("Finished");
    }
}
