package org.fivr.worker;

import org.fivr.worker.job.ControlQueue;
import org.fivr.worker.job.IJobQueue;
import org.fivr.worker.job.JobScheduler;
import org.fivr.worker.job.RedisQueue;
import org.fivr.worker.plugin.IPluginManager;
import org.fivr.worker.plugin.IWorkerPlugin;
import org.fivr.worker.plugin.PluginHelper;
import org.fivr.worker.plugin.PluginManager;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


public class Worker {

    private final WorkerSettings settings;
    private final JedisPool jedisPool;
    private final IPluginManager pluginManager;
    private final IJobQueue jobQueue;
    private final JobScheduler jobScheduler;
    private long id;
    private ControlQueue controlQueue;
    private final Calendar startupTime;

    private static Logger logger = Logger.getLogger(PluginManager.class.getName());


    public Worker(WorkerSettings settings) {
        this.settings = settings;
        this.pluginManager = new PluginManager();
        this.jedisPool = new JedisPool(settings.getRedisHost(), settings.getRedisPort());
        this.jobQueue = new RedisQueue(this.jedisPool);
        this.jobScheduler = new JobScheduler(pluginManager, jobQueue, settings.getDataPath(), settings.getWorkerThreads());
        this.startupTime = Calendar.getInstance();
    }


    public boolean process() {
        if (controlQueue.getNextCommand() == null) {
            jobScheduler.scheduleJob();
            ping();
            jobScheduler.abortJobs();
            jobScheduler.updateJobs();
            return true;
        }
        return false;
    }

    public void dispose() {
        pluginManager.dispose();
        jobScheduler.updateJobs();

        try (Jedis jedis = jedisPool.getResource()) {
            jedis.del("5r:worker:"+id);
            jedis.del("5r:control:"+id);
        }

        jedisPool.destroy();


    }

    private void ping() {
        try (Jedis jedis = jedisPool.getResource()) {
            String key = "5r:worker:" + id;
            jedis.expire(key, 5);


            Map<String, String> values = jedis.hgetAll(key);
            values.put("discFree", String.valueOf(settings.getFreeDiscSpace()));
            values.put("discTotal", String.valueOf(settings.getAvailableDiscSpace()));
            values.put("uptime", String.valueOf((Calendar.getInstance().getTimeInMillis() - startupTime.getTimeInMillis())/1000));
            jedis.hmset(key, values);
        }
    }

    public boolean setup() {
        return registerPlugins() && registerWorker();
    }

    private boolean registerWorker() {
        try(Jedis jedis = jedisPool.getResource()) {
            this.id = jedis.incr("5r:id:worker");
            controlQueue = new ControlQueue(jedisPool, String.valueOf(id));
            String key = "5r:worker:" + id;

            Map<String, String> values = new HashMap<>();
            values.put("id", String.valueOf(id));
            try {
                values.put("address", InetAddress.getLocalHost().getHostName());
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            values.put("cores", String.valueOf(settings.getWorkerThreads()));
            values.put("discFree", String.valueOf(settings.getFreeDiscSpace()));
            values.put("discTotal", String.valueOf(settings.getAvailableDiscSpace()));
            values.put("uptime", String.valueOf((Calendar.getInstance().getTimeInMillis() - startupTime.getTimeInMillis())/1000));

            jedis.hmset(key, values);
            return true;
        } catch (JedisConnectionException e) {
            logger.severe("Error registering worker: " + e.getMessage());
            return false;
        }
    }

    private boolean registerPlugins() {
        this.pluginManager.getAvailablePlugins();
        try (Jedis jedis = jedisPool.getResource()) {
            for (IWorkerPlugin p : this.pluginManager.getAvailablePlugins()) {
                jedis.sadd("5r:plugins", p.getName());
                HashMap<String, String> pluginMap = new HashMap<>();
                pluginMap.put("params", PluginHelper.getConfigParameters(p));
                pluginMap.put("type", p.getType());
                pluginMap.put("name", p.getName());
                pluginMap.put("version", p.getVersion());
                jedis.hmset("5r:plugin:" + p.getName(), pluginMap);
            }
            return true;
        } catch (JedisConnectionException e) {
            logger.severe("Error registering plugins: " + e.getMessage());
            return false;
        }
    }
}
