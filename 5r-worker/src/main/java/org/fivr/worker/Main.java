package org.fivr.worker;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, URISyntaxException {

        InputStream fis = Main.class.getClassLoader().getResourceAsStream("logging.properties");
        LogManager.getLogManager().readConfiguration(fis);

        if (args.length > 0) {
            WorkerSettings.getInstance().setRedisHost(args[0]);
        }

        if (args.length > 1) {
            WorkerSettings.getInstance().setDataPath(args[1]);
        }

        if (args.length > 2)
            WorkerSettings.getInstance().setWorkerThreads(Integer.valueOf(args[2]));

        Logger logger = LogManager.getLogManager().getLogger("");
        logger.info("Starting 5r-worker");
        Worker worker = new Worker(WorkerSettings.getInstance());
        if (worker.setup()) {
            logger.info("Successfully started 5r-worker");

            while(worker.process());

            worker.dispose();

            logger.info("Stopping 5r-worker");
            logger.info("Stopped 5r-worker");
        } else {
            logger.severe("Error starting 5r-worker");
        }


    }
}
