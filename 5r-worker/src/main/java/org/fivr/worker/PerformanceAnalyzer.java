package org.fivr.worker;

import org.fivr.worker.job.RedisQueue;
import org.fivr.worker.job.WorkerJob;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.Date;

public class PerformanceAnalyzer {


    private static JedisPool pool;
    private static WorkerSettings settings;
    private static RedisQueue queue;

    public static void main(String[] args) {

        System.out.println("Starting performance anaylzer");

        if (args.length < 5) {
            System.out.println("Argument error: threads, plugins, datasets redis-host datapath");
            System.exit(1);
        }

        String[] threads = args[0].split(",");
        String[] plugIns = args[1].split(",");
        String[] datasets = args[2].split(",");

        settings = WorkerSettings.getInstance();

        settings.setRedisHost(args[3]);
        settings.setDataPath(args[4]);

        pool = new JedisPool(settings.getRedisHost(), settings.getRedisPort());
        queue = new RedisQueue(pool);


        for (String thread : threads) {
            for (String plugin: plugIns) {
                for (String dataset: datasets) {
                    System.out.println("Starting: " + thread + " threads, " + plugin + " @ " + dataset);
                    int t = Integer.valueOf(thread);
                    createJobs(Integer.valueOf(thread), plugin, dataset);

                    settings.setWorkerThreads(t);

                    Worker w = new Worker(settings);
                    w.setup();


                    while(queue.getProcessingJobs() > 0 || queue.getTodoJobs() > 0) {
                        w.process();
                    }
                    w.dispose();
                    System.out.println("Finished jobs");
                }
            }
        }
        pool.destroy();
    }

    public static void createJobs(int thread, String plugin, String dataset) {
        WorkerSettings settings = WorkerSettings.getInstance();
        long ts = (new Date()).getTime();
        for(String video : getDataset(dataset)) {
            WorkerJob j = new WorkerJob(plugin, "input", dataset+"/"+video+"/encodings/original.mp4");
            j.setOutputDirectory(dataset+"/"+video+"/"+plugin);
            j.setOverallJob(ts+"_"+thread+"_"+plugin+"_"+dataset);
            queue.addJob(j);
        }
    }

    public static String[] getDataset(String dataset) {
        try(Jedis jedis = pool.getResource()) {
            return jedis.hget("5r:dataset:"+dataset, "videos").split(";");
        }
    }
}
