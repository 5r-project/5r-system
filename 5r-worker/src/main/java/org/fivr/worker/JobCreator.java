package org.fivr.worker;

import org.fivr.worker.job.RedisQueue;
import org.fivr.worker.job.WorkerJob;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Date;

public class JobCreator {

    private static JedisPool pool;
    private static WorkerSettings settings;
    private static RedisQueue queue;

    public static void main(String[] args) throws InterruptedException {

        System.out.println("Starting performance anaylzer");

        if (args.length < 3) {
            System.out.println("Argument error: plugins datasets redis-host");
            System.exit(1);
        }

        String[] plugIns = args[0].split(",");
        String[] datasets = args[1].split(",");

        settings = WorkerSettings.getInstance();

        settings.setRedisHost(args[2]);

        pool = new JedisPool(settings.getRedisHost(), settings.getRedisPort());
        queue = new RedisQueue(pool);


            for (String plugin: plugIns) {
                for (String dataset: datasets) {
                    System.out.println("Starting: " +  plugin + " @ " + dataset);
                    createJobs(plugin, dataset);

                    while(queue.getProcessingJobs() > 0 || queue.getTodoJobs() > 0) {
                        Thread.sleep(1000);
                    }
                    System.out.println("Finished Job");
                }
            }

            System.out.println("Finished everything");

        pool.destroy();
    }

    public static void createJobs(String plugin, String dataset) {
        WorkerSettings settings = WorkerSettings.getInstance();
        long ts = (new Date()).getTime();
        for(String video : getDataset(dataset)) {
            WorkerJob j = new WorkerJob(plugin, "input", dataset+"/"+video+"/encodings/original.mp4");
            j.setOutputDirectory(dataset+"/"+video+"/"+plugin);
            j.setOverallJob(ts+"_auto_"+plugin+"_"+dataset);
            queue.addJob(j);
        }
    }

    public static String[] getDataset(String dataset) {
        try(Jedis jedis = pool.getResource()) {
            return jedis.hget("5r:dataset:"+dataset, "videos").split(";");
        }
    }
}
