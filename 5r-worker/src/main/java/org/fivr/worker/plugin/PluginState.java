package org.fivr.worker.plugin;

public enum PluginState {
    STATE_PROCESSING("PROCESSING"), STATE_FINISHED("FINISHED"), STATE_ABORTED("ABORTED"), STATE_WAITING("WAITING"), STATE_INVALID("INVALID"), STATE_ERROR("ERROR");
    public String value;

    private PluginState(String value) {
        this.value = value;
    }


    public static PluginState fromInt(int i) {
        switch (i) {
            case 1: return STATE_WAITING;
            case 2: return STATE_PROCESSING;
            case 3: return STATE_FINISHED;
            case 4: return STATE_ABORTED;
            case 5: return STATE_ERROR;
            default: return STATE_INVALID;
        }
    }

    public static PluginState fromString(String s) {
        switch (s) {
            case "PROCESSING":
                return STATE_PROCESSING;
            case "FINISHED":
                return STATE_FINISHED;
            case "ABORTED":
                return STATE_ABORTED;
            case "WAITING":
                return STATE_WAITING;
            case "ERROR":
                return STATE_ERROR;
        }
        return STATE_INVALID;
    }
}
