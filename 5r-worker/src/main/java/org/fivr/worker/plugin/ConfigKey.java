package org.fivr.worker.plugin;


public class ConfigKey {

    private String key;
    private ConfigType type;
    private String value;

    public ConfigKey(String key, ConfigType type, String value) {
        this.key = key;
        this.type = type;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ConfigType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setType(ConfigType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return key + ":" + type + ":" + value;
    }

    public enum ConfigType {
        TypeString(1), TypeInt(2) , TypeLong(3), TypeDouble(4), TypeBool(5), TypeFile(6), TypeInvalid(0), TypeEnum(7);

        public final int value;

        private ConfigType(int value) {
            this.value = value;
        }

        public static ConfigType fromInt(int x)
        {
            switch(x) {
                case 1: return TypeString;
                case 2: return TypeInt;
                case 3: return TypeLong;
                case 4: return TypeDouble;
                case 5: return TypeBool;
                case 6: return TypeFile;
                case 7: return TypeEnum;
                default: return TypeInvalid;
            }
        }
    }
}
