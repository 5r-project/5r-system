package org.fivr.worker.plugin;

import java.util.List;
import java.util.Map;


public interface IPluginManager {

    /**
     * gets a new instance of a plugin
     * @param plugin
     * @return true, if the plugin exists and a worker is free, false otherwhise.
     */
    public IWorkerPlugin getPluginWorker(String plugin, String workingDirectory, Map<String, String> params);

    /**
     * Counterpart to getPluginWorker()
     * @param plugin worker to be released.
     */
    public void releasePluginWorker(IWorkerPlugin plugin);

    /**
     * Gets all available worker plugins
     * @return
     */
    public List<IWorkerPlugin> getAvailablePlugins();


    public void dispose();
}
