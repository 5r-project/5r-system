package org.fivr.worker.plugin;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractNativePlugin implements IWorkerPlugin {


    public abstract boolean start();
    public abstract void stop();
    public abstract double getProgress();

    public abstract void dispose();
    public abstract String getVersion();

    public abstract String getConfigValue(String key);
    public abstract boolean setConfigValue(String key, String value);

    protected abstract int getStateInt();
    protected abstract String getConfigKeyString();
    protected abstract String getGeneratedFileString();

    public PluginState getState() {
        return PluginState.fromInt(this.getStateInt());
    }

    @Override
    public List<String> getGeneratedFiles() {
        List<String> result = new ArrayList<>();
        String files = getGeneratedFileString();
        for (String s : files.split(";")) {
            result.add(s);
        }
        return result;
    }

    @Override
    public List<ConfigKey> getConfigKeys() {
        List<ConfigKey> result = new ArrayList<ConfigKey>();
        String keys = getConfigKeyString();
        for (String s : keys.split(";")) {
            String[] parts = s.split(":");
            if (parts.length == 3)
                result.add(new ConfigKey(parts[0], ConfigKey.ConfigType.fromInt(Integer.parseInt(parts[1])), parts[2]));
            else if(parts.length == 2)
                result.add(new ConfigKey(parts[0], ConfigKey.ConfigType.fromInt(Integer.parseInt(parts[1])), ""));
        }
        return result;
    }

    @Override
    public String getType() {
        return "analysis";
    }


}
