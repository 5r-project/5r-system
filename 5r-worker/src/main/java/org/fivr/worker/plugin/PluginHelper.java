package org.fivr.worker.plugin;

import java.util.ArrayList;
import java.util.List;


public class PluginHelper {

    public static List<String> getFileConfigKeys(IWorkerPlugin plugin) {
        List<String> result = new ArrayList<>();
        for(ConfigKey c : plugin.getConfigKeys()) {
            if (c.getType() == ConfigKey.ConfigType.TypeFile)
                result.add(c.getKey());
        }
        return result;
    }

    public static String getConfigParameters(IWorkerPlugin plugin) {
        StringBuffer sb = new StringBuffer();

        for(ConfigKey c : plugin.getConfigKeys()) {
            if (sb.length() > 0) {
                sb.append(";");
            }
            sb.append(c.getKey());
            sb.append(":");
            sb.append(c.getType().value);
            sb.append(":");
            sb.append(c.getValue());
        }

        return sb.toString();
    }
}
