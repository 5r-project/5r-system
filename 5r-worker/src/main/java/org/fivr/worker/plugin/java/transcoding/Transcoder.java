package org.fivr.worker.plugin.java.transcoding;

import org.fivr.worker.plugin.AbstractJavaPlugin;
import org.fivr.worker.plugin.ConfigKey;

import java.io.IOException;


public class Transcoder extends AbstractJavaPlugin {

    private final String size;
    private FFMPEGPiper piper;
    private String inputFile;
    private String outputFile;

    public Transcoder(String workingDirectory, String size) {
        super("1.0.0", workingDirectory);
        this.size = size;
        this.configKeys.add(new ConfigKey("input", ConfigKey.ConfigType.TypeFile, ""));
        this.outputFile = workingDirectory + "/" + "encoding_"+size+".mp4";

        this.generatedFiles.add(0, outputFile);
    }

    @Override
    public double getProgress() {
        if (piper == null)
            return 0;
        return piper.getProgress();
    }

    @Override
    public boolean setConfigValue(String key, String value) {
        switch (key) {
            case "input":
                inputFile = value;
                return true;
            default:
                return false;
        }
    }

    @Override
    public String getType() {
        return "preprocessing";
    }

    @Override
    public void run() {

        try {
            while(run && piper.process());
        } catch (InterruptedException e) {
            e.printStackTrace();
            setInfo(e.getMessage());
        }

    }

    @Override
    protected boolean init() {
        String[] command = {"-y", "-i", "-", "-vcodec", "h264", "-an", "-s", this.size };
        try {
            piper = new FFMPEGPiper(inputFile, outputFile, command);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
