package org.fivr.worker.plugin.java;

import org.fivr.worker.plugin.java.transcoding.Transcoder;


public class Transcode720p extends Transcoder {

    public Transcode720p(String workingDirectory) {
        super(workingDirectory, "1280x720");
    }
}
