package org.fivr.worker.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

public class PluginManager implements IPluginManager {

    private static Logger logger = Logger.getLogger(PluginManager.class.getName());

    private List<IWorkerPlugin> runningPlugins;

    private List<IWorkerPlugin> availablePlugins;

    public PluginManager() {
        this.runningPlugins = Collections.synchronizedList(new ArrayList<IWorkerPlugin>());

        this.availablePlugins = new ArrayList<>();

        this.scanAvailablePlugins("cxx");
        this.scanAvailablePlugins("java");

        StringBuffer sb = new StringBuffer();
        for(IWorkerPlugin p : availablePlugins) {
            sb.append(p.getName()+ ", ");
        }

        logger.info("Detected PlugIns: " + sb.toString());

    }

    public IWorkerPlugin getPluginWorker(String plugin, String workingDirectory, Map<String, String> params) {
        IWorkerPlugin p = getPlugin(plugin, workingDirectory, params);
        if (p != null)
            runningPlugins.add(p);
        return p;
    }

    public void releasePluginWorker(IWorkerPlugin plugin) {
        if (plugin != null) {
            plugin.dispose();
            runningPlugins.remove(plugin);
        }
    }

    @Override
    public List<IWorkerPlugin> getAvailablePlugins() {
        return availablePlugins;
    }

    @Override
    public void dispose() {
        for (IWorkerPlugin p : availablePlugins)
            p.dispose();

        for (IWorkerPlugin p : runningPlugins)
            p.dispose();

    }

    static String[] getResourceListing(Class clazz, String path) throws URISyntaxException, IOException {
        URL dirURL = clazz.getClassLoader().getResource(path);
        if (dirURL != null && dirURL.getProtocol().equals("file")) {
            return new File(dirURL.toURI()).list();
        }

        if (dirURL == null) {
            String me = clazz.getName().replace(".", "/")+".class";
            dirURL = clazz.getClassLoader().getResource(me);
        }

        if (dirURL.getProtocol().equals("jar")) {
            String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!")); //strip out only the JAR file
            JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
            Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
            Set<String> result = new HashSet<String>(); //avoid duplicates in case it is a subdirectory
            while(entries.hasMoreElements()) {
                String name = entries.nextElement().getName();
                if (name.startsWith(path)) { //filter according to the path
                    String entry = name.substring(path.length());
                    int checkSubdir = entry.indexOf("/");
                    if (checkSubdir < 0) {
                        result.add(entry);
                    }
                }
            }
            return result.toArray(new String[result.size()]);
        }
        return new String[]{};
    }

    private void scanAvailablePlugins(String path) {
        String scannedPath = "org/fivr/worker/plugin/"+ path + "/";

        try {
            String[] classes = getResourceListing(PluginManager.class, scannedPath);
            for(String c : classes) {
                if (c.contains(".class") && !c.contains("$")) {
                    String pluginName = path + "." + c.substring(0, c.length() - 6);
                    IWorkerPlugin plugin = createPlugin(pluginName, "");
                    availablePlugins.add(plugin);
                }
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    private static IWorkerPlugin createPlugin(String plugin, String workingDirectory) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> clazz = Class.forName("org.fivr.worker.plugin." +plugin);
        Constructor<?> ctor = clazz.getConstructor(String.class);
        Object object = ctor.newInstance(new Object[] {workingDirectory});
        return (IWorkerPlugin) object;
    }

    private static IWorkerPlugin getPlugin(String plugin, String workingDirectory, Map<String, String> params) {
        try {
            IWorkerPlugin result = createPlugin(plugin, workingDirectory);
            for(Map.Entry<String,String> p : params.entrySet()) {
                result.setConfigValue(p.getKey(), p.getValue());
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }
}
