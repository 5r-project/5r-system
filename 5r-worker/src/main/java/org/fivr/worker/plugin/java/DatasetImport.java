package org.fivr.worker.plugin.java;

import org.fivr.worker.PathHelper;
import org.fivr.worker.WorkerSettings;
import org.fivr.worker.job.RedisQueue;
import org.fivr.worker.job.WorkerJob;
import org.fivr.worker.plugin.AbstractJavaPlugin;
import org.fivr.worker.plugin.ConfigKey;
import org.fivr.worker.plugin.PluginState;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;


public class DatasetImport extends AbstractJavaPlugin {

    private final JedisPool jedisPool;
    private final RedisQueue jobQueue;
    private String directory;
    private boolean transcode720p;
    private boolean transcode480p;
    private boolean transcodeQVGA;

    private List<String> params;

    private double progress = 0;

    private List<String> pps;
    private HashSet<String> encodings;

    public DatasetImport(String workingDirectory) {
        super("1.0.0", workingDirectory);
        this.params = new ArrayList<>();
        this.configKeys.add(new ConfigKey("directory", ConfigKey.ConfigType.TypeFile, ""));
        this.configKeys.add(new ConfigKey("java.Transcode720p", ConfigKey.ConfigType.TypeBool, "false"));
        this.configKeys.add(new ConfigKey("java.Transcode480p", ConfigKey.ConfigType.TypeBool, "false"));
        this.configKeys.add(new ConfigKey("java.TranscodeQVGA", ConfigKey.ConfigType.TypeBool, "false"));

        this.jedisPool = new JedisPool(WorkerSettings.getInstance().getRedisHost(), WorkerSettings.getInstance().getRedisPort());
        this.jobQueue = new RedisQueue(this.jedisPool);

        pps = new ArrayList<String>();
        encodings = new HashSet<>();
        encodings.add("original.mp4");

    }

    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public boolean setConfigValue(String key, String value) {
        if (key.equals("directory")) {
            directory = value;
        } else {
            if (Boolean.parseBoolean(value)) {
                params.add(key);
            }
        }
        return true;
    }

    @Override
    public String getType() {
        return "import";
    }

    @Override
    public void run() {

        File[] files = new File(directory).listFiles();

        importFiles(files);

        while(pps.size() > 0 && run) {
            double sum = 0;
            ArrayList<String> toRemove = new ArrayList<>();
            for (String id : pps) {
                WorkerJob j = jobQueue.getJob(id);
                sum += j.getProgress();
                if (j.getState() != PluginState.STATE_PROCESSING && j.getState() != PluginState.STATE_WAITING) {
                    toRemove.add(j.getId());
                    encodings.addAll(j.getOutputFiles());

                }
            }
            progress = sum / (double)pps.size();
            pps.removeAll(toRemove);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        createDataset(files);

        progress = 1.0;
    }

    private void createDataset(File[] files) {
        try(Jedis jedis = jedisPool.getResource()) {
            long id = jedis.incr("5r:id:dataset");
            String dir = directory.substring(directory.lastIndexOf("/") + 1);
            jedis.zadd("5r:datasets", id, dir);
            HashMap<String, String> values = new HashMap<>();
            StringBuilder sb = new StringBuilder();
            for(String s : encodings) {
                sb.append(s);
                sb.append(";");
            }

            values.put("encodings", sb.toString());

            sb = new StringBuilder();
            for (File f : files) {
                sb.append(f.getName().replaceFirst("[.][^.]+$", "").substring(f.getName().lastIndexOf("/")+1));
                sb.append(";");
            }
            values.put("directory", dir);
            values.put("videos", sb.toString());
            jedis.hmset("5r:dataset:" + dir, values);
        }
    }

    private void importFiles(File[] files) {
        for (int i = 0; i < files.length && run; i++) {
            File f = files[i];
            if (f.isFile()) {
                importFile(f);
            }
            if (pps.size() == 0)
                progress = (double)(i+1) / (double)f.length();
        }
    }

    private void importFile(File f) {
        String directoryName = directory + "/" + f.getName().replaceFirst("[.][^.]+$", "");
        new File(directoryName + "/encodings").mkdirs();
        try {
            Path targetPath = Paths.get(directoryName, "encodings", "original.mp4");
            Files.move(Paths.get(f.getAbsolutePath()), targetPath, StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING);

            for(String p : params) {
                WorkerJob job = new WorkerJob(p, "input", PathHelper.toRelativePath(targetPath.toString()));
                job.setOutputDirectory(PathHelper.toRelativePath(Paths.get(directoryName, "encodings").toString()));
                pps.add(jobQueue.addJob(job));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean init() {
        return true;
    }
}
