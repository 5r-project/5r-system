package org.fivr.worker.plugin.java;

import org.fivr.worker.plugin.java.transcoding.Transcoder;

public class TranscodeQVGA extends Transcoder {

    public TranscodeQVGA(String workingDirectory) {
        super(workingDirectory, "320x240");
    }
}
