package org.fivr.worker.plugin.java;

import org.fivr.worker.plugin.java.transcoding.Transcoder;


public class Transcode480p extends Transcoder {

    public Transcode480p(String workingDirectory) {
        super(workingDirectory, "800x480");
    }
}
