package org.fivr.worker.plugin.java.transcoding;

import java.io.*;

public class FFMPEGPiper {

    private final BufferedInputStream bis;
    private final ProcessBuilder pb;
    private final Process p;
    private final OutputStream ffmpegInput;
    private File f;
    private long totalSize = 0;
    private long written = 0;
    private byte[] buffer;

    public FFMPEGPiper(String input, String output, String[] params) throws IOException {
        f = new File(input);
        totalSize = f.length();
        bis = new BufferedInputStream(new FileInputStream(f));
        String[] command = new String[params.length + 2];
        command[0] = "ffmpeg";
        for (int i = 1; i < command.length-1; i++) {
            command[i] = params[i-1];
        }
        command[params.length+1] = output;

        pb = new ProcessBuilder(command);
        //pb.redirectErrorStream(true);
        pb.redirectOutput();
        pb.redirectInput(ProcessBuilder.Redirect.PIPE);

        buffer = new byte[4096];
        p = pb.start();
        ffmpegInput = p.getOutputStream();
    }

    public boolean process() throws InterruptedException {
        try {
            int read = bis.read(buffer);
            if (read > 0) {
                ffmpegInput.write(buffer);
                written += buffer.length;
                return true;
            }
            written = totalSize;
            ffmpegInput.close();
            p.waitFor();
            bis.close();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public double getProgress() {
        return written / (double) totalSize;
    }

}
