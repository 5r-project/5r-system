package org.fivr.worker.plugin;

import java.util.List;

public interface IWorkerPlugin {

    boolean start();
    void stop();
    double getProgress();
    PluginState getState();

    List<String> getGeneratedFiles();
    List<ConfigKey> getConfigKeys();
    boolean setConfigValue(String key, String value);

    void dispose();
    String getVersion();
    String getName();
    String getType();
    String getInfo();
}
