package org.fivr.worker.plugin;


public enum PluginType {
    PREPROCESSING, ANALYSIS, IMPORT
}
