package org.fivr.worker.plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractJavaPlugin implements IWorkerPlugin, Runnable {

    private Thread t;
    private final String version;
    private PluginState state;
    private String info;
    protected final String workingDirectory;
    protected List<String> generatedFiles;
    protected List<ConfigKey> configKeys;

    protected boolean run;

    public AbstractJavaPlugin(String version, String workingDirectory)
    {
        this.version = version;
        this.workingDirectory = workingDirectory;

        this.generatedFiles = Collections.synchronizedList(new ArrayList<String>());
        this.configKeys = new ArrayList<ConfigKey>();
        state = PluginState.STATE_WAITING;
        info = "";
    }

    @Override
    public boolean start() {
        if (!run && init()) {
            run = true;
            t = new Thread(this);
            t.start();
            state = PluginState.STATE_PROCESSING;
        } else {
            state = PluginState.STATE_ERROR;
        }
        return run;
    }

    @Override
    public void stop() {
        run = false;
        if (t != null) {
            try {
                if (getProgress() < 1.0)
                    state = PluginState.STATE_ABORTED;
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public PluginState getState() {
        if (state == PluginState.STATE_PROCESSING && getProgress() >= 1.0) {
            state = PluginState.STATE_FINISHED;
        }
        return state;
    }

    @Override
    public List<String> getGeneratedFiles() {
        return generatedFiles;
    }

    @Override
    public List<ConfigKey> getConfigKeys() {
        return configKeys;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getName() {
        return "java." + this.getClass().getSimpleName();
    }

    @Override
    public void dispose() {
        stop();
    }

    @Override
    public String getInfo() {
        return info;
    }

    protected void setInfo(String info) {
        this.info = info;
    }

    protected abstract boolean init();
}
