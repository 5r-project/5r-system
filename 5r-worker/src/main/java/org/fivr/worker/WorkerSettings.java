package org.fivr.worker;

import java.io.File;


public class WorkerSettings {

    private String redisHost;
    private int redisPort;
    private long queueInterval;
    private int workerThreads;
    private String dataPath;

    private static WorkerSettings instance = new WorkerSettings();

    public static WorkerSettings getInstance() {
        return instance;
    }

    private WorkerSettings() {
        redisHost = "localhost";
        redisPort = 6379;
        queueInterval = 1000;
        workerThreads = Runtime.getRuntime().availableProcessors();
        dataPath = "/media/fivr";
    }

    public String getRedisHost() {
        return redisHost;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public long getQueueInterval() {
        return queueInterval;
    }

    public int getWorkerThreads() {
        return workerThreads;
    }

    public void setWorkerThreads(int threads) {
        workerThreads = threads;
    }

    public String getDataPath() {
        return dataPath;
    }

    public void setRedisHost(String host) {
        this.redisHost = host;
    }

    public void setDataPath(String path) {
        this.dataPath = path;
    }

    public long getAvailableDiscSpace() {
        return new File("/").getTotalSpace();
    }

    public long getFreeDiscSpace() {
        return new File("/").getFreeSpace();
    }
}
