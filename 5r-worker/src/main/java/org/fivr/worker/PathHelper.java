package org.fivr.worker;


public class PathHelper {

    public static String toRelativePath(String absolutePath) {
        return absolutePath.replaceFirst(WorkerSettings.getInstance().getDataPath(), "");
    }

    public static String toAbsolutePath(String relativePath) {
        return WorkerSettings.getInstance().getDataPath() + relativePath;
    }
}
