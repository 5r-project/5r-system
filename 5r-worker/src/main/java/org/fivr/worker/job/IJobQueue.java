package org.fivr.worker.job;

import java.util.Set;


public interface IJobQueue {
    void updateJob(WorkerJob job);
    void finishJob(WorkerJob job);
    public WorkerJob getWaitingJob(int timeout);
    public Set<String> getAbortJobs();
    public void removeAbortJobs(Set<String> jobs);
}
