package org.fivr.worker.job;

import org.fivr.worker.plugin.IWorkerPlugin;
import org.fivr.worker.plugin.PluginState;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkerJob extends HashMap<String,String> {

    public static final String ID = "id";
    public static final String PLUGIN = "plugin";
    public static final String PARAMS = "params";
    public static final String PROGRESS = "progress";
    public static final String STATE = "state";
    public static final String OUTPUT_FILES = "files";
    public static final String OUTPUT_DIRECTORY = "directory";
    public static final String INFO = "info";
    public static final String TIME_START = "timeStart";
    public static final String TIME_END = "timeStop";

    private IWorkerPlugin plugin;

    public WorkerJob(Map<String, String> job) {
        super(job);
    }

    public WorkerJob(String plugin, Map<String, String> params) {
        this.put(PLUGIN, plugin);
        this.setParams(params);
        this.setState(PluginState.STATE_WAITING);
        this.setProgress(0);
        this.put(INFO, "");
        this.setOutputDirectory("");
    }

    public WorkerJob(String plugin, String ... params) {
        this.put(PLUGIN, plugin);
        this.setState(PluginState.STATE_WAITING);
        this.setParams(params);
        this.setProgress(0);
        this.setOutputDirectory("");
        this.put(INFO, "");
    }

    public void setId(long id) {
        this.put(ID, String.valueOf(id));
    }

    public String getId() {
        return this.get(ID);
    }

    public String getPlugin() {
        return this.get(PLUGIN);
    }

    public IWorkerPlugin getExecutingPlugin() {
        return plugin;
    }

    public void setExecutingPlugin(IWorkerPlugin plugin) {
        this.plugin = plugin;
    }

    public Map<String, String> getParams() {
        Map<String, String> result = new HashMap<>();
        if (this.get(PARAMS) != null) {
            for(String s : this.get(PARAMS).split(";")) {
                String[] p = s.split(":");
                if (p.length == 2)
                    result.put(p[0], p[1]);
            }
        }
        return result;
    }

    public List<String> getOutputFiles() {
        List<String> result = new ArrayList<>();

        for(String s : this.get(OUTPUT_FILES).split(";")) {
            result.add(s);
        }
        return result;
    }

    public void setOutputFiles(List<String> files) {
        StringBuffer sb = new StringBuffer();
        for (String file : files) {
            if (file.length() > 0)
            {
                if (file.lastIndexOf(getOutputDirectory()) > -1)
                {
                    int idx = file.lastIndexOf(getOutputDirectory()) + getOutputDirectory().length() +1;
                    file = file.substring(idx);
                }
                sb.append(file);
                sb.append(";");
            }
        }
        this.put(OUTPUT_FILES, sb.toString());
    }

    public double getProgress() {
        return Double.parseDouble(this.get(PROGRESS));
    }

    public PluginState getState() {
        return PluginState.fromString(this.get(STATE));
    }

    public void setProgress(double progress) {
        this.put(PROGRESS, String.valueOf(progress));
    }

    public void setState(PluginState state) {
        this.put(STATE, state.value);
    }

    public void setParams(Map<String,String> params) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> p : params.entrySet()) {
            sb.append(p.getKey());
            sb.append(":");
            sb.append(p.getValue());
            sb.append(";");
        }
        String s = sb.toString();
        if (s.length() > 0)
            this.put(PARAMS, s.substring(0, s.length()-1));
        else
            this.put(PARAMS, "");
    }

    public void setParams(String ... params) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < params.length; i+=2) {
            sb.append(params[i]);
            sb.append(":");
            sb.append(params[i+1]);
            sb.append(";");
        }
        String s = sb.toString();
        if (s.length() > 0)
            this.put(PARAMS, s.substring(0, s.length()-1));
        else
            this.put(PARAMS, "");
    }

    public String getOutputDirectory() {
        return this.get(OUTPUT_DIRECTORY);
    }

    public String getInfo() {
        return this.get(INFO);
    }

    public void setInfo(String info) {
        this.put(INFO, info);
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.00");
        return "Job: " + getId() + ", " + getPlugin() + ", " + getParams() + ", " + df.format(getProgress()*100) + "%, " + getState();
    }

    public void setOutputDirectory(String outputDirectory) {
        this.put(OUTPUT_DIRECTORY, outputDirectory);
    }

    public void setStartTime(long startTime) {
        this.put(TIME_START, String.valueOf(startTime));
    }

    public void setEndTime(long endTime) {
        this.put(TIME_END, String.valueOf(endTime));
    }

    public void setOverallJob(String job) {
        this.put("overallJob", job);
    }

    public void update() {
        if (plugin != null) {
            setProgress(plugin.getProgress());
            setState(plugin.getState());
            setOutputFiles(plugin.getGeneratedFiles());
            setInfo(plugin.getInfo());
        }
    }
}
