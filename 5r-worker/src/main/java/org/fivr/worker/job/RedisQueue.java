package org.fivr.worker.job;
import org.fivr.worker.plugin.PluginState;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;


public class RedisQueue implements IJobQueue {

    public static final String QUEUE_TODO = "5r:queue:todo";
    public static final String QUEUE_PROCESSING = "5r:queue:processing";
    public static final String QUEUE_ABORT = "5r:queue:abort";
    public static final String QUEUE_DONE = "5r:queue:done";
    public static final String JOB_KEY_PREFIX = "5r:job:";
    public static final String JOB_ID = "5r:id:job";

    private JedisPool pool;

    public RedisQueue(JedisPool pool) {
        this.pool = pool;
    }

    public WorkerJob getWaitingJob(int timeout) {
        try (Jedis jedis = pool.getResource()) {
            String jobId = jedis.brpoplpush(QUEUE_TODO, QUEUE_PROCESSING, timeout);
            if (jobId == null)
                return null;

            WorkerJob job = new WorkerJob(jedis.hgetAll(JOB_KEY_PREFIX + jobId));
            job.setState(PluginState.STATE_PROCESSING);
            updateJob(job);
            return job;
        }
    }

    @Override
    public Set<String> getAbortJobs() {
        try(Jedis jedis = pool.getResource()) {
            return jedis.smembers(QUEUE_ABORT);
        }
    }

    @Override
    public void removeAbortJobs(Set<String> jobs) {
        try(Jedis jedis = pool.getResource()) {
            for (String s : jobs) {
                jedis.srem(QUEUE_ABORT, s);
            }
        }
    }

    public long getTodoJobs() {
        try(Jedis jedis = pool.getResource()) {
            return jedis.llen(QUEUE_TODO);
        }
    }

    public long getProcessingJobs() {
        try(Jedis jedis = pool.getResource()) {
            return jedis.llen(QUEUE_PROCESSING);
        }
    }

    public WorkerJob getJob(String id) {
        try (Jedis jedis = pool.getResource()) {
            return new WorkerJob(jedis.hgetAll(JOB_KEY_PREFIX + id));
        }
    }

    public String addJob(WorkerJob job) {
        try (Jedis jedis = pool.getResource()) {
            long id = jedis.incr(JOB_ID);
            job.setId(id);
            job.setStartTime(0);
            job.setEndTime(0);
            job.setInfo("");
            job.setState(PluginState.STATE_WAITING);
            updateJob(job);
            jedis.rpush(QUEUE_TODO, job.getId());
            return String.valueOf(id);
        }
    }

    @Override
    public void updateJob(WorkerJob job) {
        try (Jedis jedis = pool.getResource()) {
            jedis.hmset(JOB_KEY_PREFIX + job.getId(), job);
        }
    }

    @Override
    public void finishJob(WorkerJob job) {
        try (Jedis jedis = pool.getResource()) {
            jedis.rpush(QUEUE_DONE, job.getId());
            jedis.lrem(QUEUE_PROCESSING, 0, job.getId());
        }
    }

    public void removeJob(WorkerJob job) {
        try (Jedis jedis = pool.getResource()) {
            jedis.hdel(JOB_KEY_PREFIX + job.getId(), WorkerJob.ID, WorkerJob.PARAMS, WorkerJob.PLUGIN, WorkerJob.PROGRESS, WorkerJob.STATE);
            jedis.lrem(QUEUE_DONE, 0, job.getId());
            jedis.lrem(QUEUE_TODO, 0, job.getId());
        }
    }
}
