package org.fivr.worker.job;

import org.fivr.worker.plugin.IPluginManager;
import org.fivr.worker.plugin.IWorkerPlugin;
import org.fivr.worker.plugin.PluginHelper;
import org.fivr.worker.plugin.PluginState;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;

public class JobScheduler {
    private final int maxParallelThreads;
    private IPluginManager pluginManager;
    private IJobQueue queue;

    private Map<String, WorkerJob> jobs;
    private List<String> toRelease;

    private String mountPath;

    private static Logger logger = Logger.getLogger(JobScheduler.class.getName());


    public JobScheduler(IPluginManager pluginManager, IJobQueue queue, String mountPath, int maxParallelThreads) {
        jobs = Collections.synchronizedMap(new HashMap<String, WorkerJob>());
        toRelease = new ArrayList<>();
        this.queue = queue;
        this.pluginManager = pluginManager;
        this.mountPath = mountPath;
        this.maxParallelThreads = maxParallelThreads;
    }

    public void updateJobs() {
        for(Map.Entry<String, WorkerJob> job : jobs.entrySet()) {
            WorkerJob j = job.getValue();
            j.update();
            queue.updateJob(j);
            logger.info(j.toString());
            if (j.getState() != PluginState.STATE_PROCESSING && j.getState() != PluginState.STATE_WAITING) {
                j.setEndTime(Calendar.getInstance().getTimeInMillis());
                queue.updateJob(j);
                queue.finishJob(j);
                pluginManager.releasePluginWorker(j.getExecutingPlugin());
                toRelease.add(j.getId());
            }


        }

        for (String j : toRelease)
            jobs.remove(j);
        toRelease.clear();

    }

    public void abortJobs() {
        Set<String> abortIds = queue.getAbortJobs();
        Set<String> toRemove = new HashSet<>();
        for(String id : abortIds) {
            if (jobs.containsKey(id)) {
                WorkerJob j = jobs.get(id);
                j.getExecutingPlugin().stop();
                toRemove.add(id);
            }
        }

        queue.removeAbortJobs(toRemove);

    }

    private boolean prepareJob(WorkerJob job) {
        IWorkerPlugin plugin = pluginManager.getPluginWorker(job.getPlugin(), mountPath + "/" + job.getOutputDirectory(), job.getParams());
        try {
            if (plugin != null) {
                job.setExecutingPlugin(plugin);
                Path target = Paths.get(mountPath, job.getOutputDirectory());

                if (Files.isSymbolicLink(target))
                    target = Files.readSymbolicLink(target);

                if (!Files.exists(target));
                    Files.createDirectories(target);
                List<String> fileKeys = PluginHelper.getFileConfigKeys(plugin);
                for (String key : fileKeys) {
                    plugin.setConfigValue(key, mountPath + "/" + job.getParams().get(key));
                }
                job.setStartTime(Calendar.getInstance().getTimeInMillis());
                if (plugin.start()) {
                    return true;
                } else {
                    job.setInfo("Could not start PlugIn");
                    logger.severe("Could not start PlugIn for " + job);
                }
            }
        } catch (IOException e)  {
            job.setInfo("Could not create OutputDirectory: " + Paths.get(mountPath, job.getOutputDirectory()).toString());
            logger.severe("Could not create OutputDirectory: " + Paths.get(mountPath, job.getOutputDirectory()).toString());
        }
        return false;
    }

    public void scheduleJob() {
        if (jobs.size() < maxParallelThreads) {
            WorkerJob job = queue.getWaitingJob(1);
            if (job != null) {
                prepareJob(job);
                jobs.put(job.getId(), job);
            }
        } else {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
